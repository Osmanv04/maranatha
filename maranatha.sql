-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2017 a las 11:24:19
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `maranatha`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargafamiliar`
--

CREATE TABLE `cargafamiliar` (
  `id` int(10) UNSIGNED NOT NULL,
  `parentesco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombrep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidop` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `persona_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `celulas`
--

CREATE TABLE `celulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `red_id` int(10) UNSIGNED NOT NULL,
  `sector_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `id` int(10) UNSIGNED NOT NULL,
  `avenida` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `calle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nroCasa` int(11) NOT NULL,
  `edificio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `municipio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edocivil`
--

CREATE TABLE `edocivil` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `edocivil`
--

INSERT INTO `edocivil` (`id`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Soltero', '2017-05-30 02:30:18', '2017-05-30 02:30:18'),
(2, 'Casado', '2017-05-30 02:30:18', '2017-05-30 02:30:18'),
(3, 'Divorciado', '2017-05-30 02:30:18', '2017-05-30 02:30:18'),
(4, 'Viudo', '2017-05-30 02:30:18', '2017-05-30 02:30:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Amazonas', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(2, 'Anzoategui', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(3, 'Apure', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(4, 'Aragua', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(5, 'Barinas', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(6, 'Bolivar', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(7, 'Carabobo', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(8, 'Cojedes', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(9, 'Delta Amacuro', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(10, 'Distrito Capital ', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(11, 'Falcon', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(12, 'Guarico ', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(13, 'Lara', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(14, 'Merida', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(15, 'Miranda', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(16, 'Monagas', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(17, 'Nueva Esparta', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(18, 'Portuguesa', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(19, 'Sucre', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(20, 'Tachira', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(21, 'Trujillo', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(22, 'Vargas', '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(23, 'Yaracuy', '2017-05-30 02:30:18', '2017-05-30 02:30:18'),
(24, 'Zulia', '2017-05-30 02:30:18', '2017-05-30 02:30:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_01_14_162026_EdoCivil', 1),
('2016_01_14_195530_create_red_table', 1),
('2016_05_15_230251_create_sectors_table', 1),
('2016_06_17_122657_create_celulas_table', 1),
('2016_11_14_210643_create_telefono_table', 1),
('2016_12_14_183102_create_estado_table', 1),
('2016_12_14_192812_create_direccion_table', 1),
('2016_12_14_201151_create_persona_table', 1),
('2016_12_14_201452_create_cargaFamiliar_table', 1),
('2016_12_15_19632_create_pastores_table', 1),
('2017_02_16_135024_create_public_services_table', 1),
('2017_02_16_135055_create_peticion_oracions_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pastores`
--

CREATE TABLE `pastores` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `red_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pastores`
--

INSERT INTO `pastores` (`id`, `nombre`, `apellido`, `red_id`, `created_at`, `updated_at`) VALUES
(1, 'Omar', 'Paraguan', 2, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(2, 'Javier', 'Reyes', 2, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(3, 'Emely', 'Paraguan', 3, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(4, 'Isabel', 'Ramos', 3, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(5, 'Orlando', 'Alfonso', 4, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(6, 'Marina', 'de Alfonso', 4, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(7, 'Luis', 'Santaella', 5, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(8, 'Erick', 'Castro', 1, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(9, 'Marisela', 'de Castro', 1, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(10, 'Pedro', 'Gonzalez', 1, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(11, 'Yadirse', 'de Gonzalez', 1, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(12, 'Pedro', 'Henning', 6, '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(13, 'Elina', 'de Henning', 6, '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(14, 'Cipriano', 'Hernandez', 7, '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(15, 'Aida', 'de Hernandez', 7, '2017-05-30 02:30:17', '2017-05-30 02:30:17'),
(16, 'Omar', 'Gonzalez', 8, '2017-05-30 02:38:38', '2017-05-30 02:38:38'),
(17, 'zxz', 'xzxzx', 9, '2017-05-30 02:47:00', '2017-05-30 02:47:00'),
(18, 'p1', 'p1', 19, '2017-05-30 03:05:51', '2017-05-30 03:05:51'),
(19, 'p2', 'p2', 19, '2017-05-30 03:05:51', '2017-05-30 03:05:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cedula` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `ocupacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bautizo` tinyint(1) NOT NULL,
  `esLider` tinyint(1) NOT NULL,
  `otraIglesia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tiempoIglesia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_red` tinyint(1) NOT NULL,
  `edocivil_id` int(10) UNSIGNED NOT NULL,
  `direccion_id` int(10) UNSIGNED NOT NULL,
  `red_id` int(10) UNSIGNED DEFAULT NULL,
  `celula_id` int(10) UNSIGNED DEFAULT NULL,
  `tlf_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peticion_oracions`
--

CREATE TABLE `peticion_oracions` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `public_services`
--

CREATE TABLE `public_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `encargado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contacto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `red`
--

CREATE TABLE `red` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `red`
--

INSERT INTO `red` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Empresarios', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(2, 'Hombres', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(3, 'Mujeres', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(4, 'Jóvenes', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(5, 'Niños y prejuveniles', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(6, 'Urbana', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(7, 'Matrimonios', '2017-05-30 02:30:16', '2017-05-30 02:30:16'),
(8, 'Tercera edad', '2017-05-30 02:38:38', '2017-05-30 02:38:38'),
(9, 'zxz', '2017-05-30 02:47:00', '2017-05-30 02:47:00'),
(11, 'kkkkkk', '2017-05-30 02:53:22', '2017-05-30 02:53:22'),
(12, 'kkkkkk', '2017-05-30 02:53:23', '2017-05-30 02:53:23'),
(13, 'ooooo', '2017-05-30 02:53:43', '2017-05-30 02:53:43'),
(14, 'ooooo', '2017-05-30 02:54:12', '2017-05-30 02:54:12'),
(15, 'ooooo', '2017-05-30 02:55:17', '2017-05-30 02:55:17'),
(16, 'ooooo', '2017-05-30 02:56:45', '2017-05-30 02:56:45'),
(17, 'ooooo', '2017-05-30 02:58:45', '2017-05-30 02:58:45'),
(18, 'ooooo', '2017-05-30 03:00:30', '2017-05-30 03:00:30'),
(19, 'Red 1', '2017-05-30 03:05:51', '2017-05-30 03:05:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sectors`
--

CREATE TABLE `sectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreSector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `id` int(10) UNSIGNED NOT NULL,
  `movil` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `casa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cargafamiliar`
--
ALTER TABLE `cargafamiliar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargafamiliar_persona_id_foreign` (`persona_id`);

--
-- Indices de la tabla `celulas`
--
ALTER TABLE `celulas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `celulas_red_id_foreign` (`red_id`),
  ADD KEY `celulas_sector_id_foreign` (`sector_id`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direccion_estado_id_foreign` (`estado_id`);

--
-- Indices de la tabla `edocivil`
--
ALTER TABLE `edocivil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pastores`
--
ALTER TABLE `pastores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pastores_red_id_foreign` (`red_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persona_cedula_unique` (`cedula`),
  ADD KEY `persona_direccion_id_foreign` (`direccion_id`),
  ADD KEY `persona_red_id_foreign` (`red_id`),
  ADD KEY `persona_tlf_id_foreign` (`tlf_id`),
  ADD KEY `persona_edocivil_id_foreign` (`edocivil_id`),
  ADD KEY `persona_celula_id_foreign` (`celula_id`);

--
-- Indices de la tabla `peticion_oracions`
--
ALTER TABLE `peticion_oracions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `public_services`
--
ALTER TABLE `public_services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `red`
--
ALTER TABLE `red`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sectors`
--
ALTER TABLE `sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargafamiliar`
--
ALTER TABLE `cargafamiliar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `celulas`
--
ALTER TABLE `celulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `edocivil`
--
ALTER TABLE `edocivil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `pastores`
--
ALTER TABLE `pastores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `peticion_oracions`
--
ALTER TABLE `peticion_oracions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `public_services`
--
ALTER TABLE `public_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `red`
--
ALTER TABLE `red`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `sectors`
--
ALTER TABLE `sectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cargafamiliar`
--
ALTER TABLE `cargafamiliar`
  ADD CONSTRAINT `cargafamiliar_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`);

--
-- Filtros para la tabla `celulas`
--
ALTER TABLE `celulas`
  ADD CONSTRAINT `celulas_red_id_foreign` FOREIGN KEY (`red_id`) REFERENCES `red` (`id`),
  ADD CONSTRAINT `celulas_sector_id_foreign` FOREIGN KEY (`sector_id`) REFERENCES `sectors` (`id`);

--
-- Filtros para la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD CONSTRAINT `direccion_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`);

--
-- Filtros para la tabla `pastores`
--
ALTER TABLE `pastores`
  ADD CONSTRAINT `pastores_red_id_foreign` FOREIGN KEY (`red_id`) REFERENCES `red` (`id`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_celula_id_foreign` FOREIGN KEY (`celula_id`) REFERENCES `celulas` (`id`),
  ADD CONSTRAINT `persona_direccion_id_foreign` FOREIGN KEY (`direccion_id`) REFERENCES `direccion` (`id`),
  ADD CONSTRAINT `persona_edocivil_id_foreign` FOREIGN KEY (`edocivil_id`) REFERENCES `edocivil` (`id`),
  ADD CONSTRAINT `persona_red_id_foreign` FOREIGN KEY (`red_id`) REFERENCES `red` (`id`),
  ADD CONSTRAINT `persona_tlf_id_foreign` FOREIGN KEY (`tlf_id`) REFERENCES `telefono` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
