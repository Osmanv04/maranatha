/**
 * Created by Osman Jose on 2/3/2017.
 */
$(document).ready(function() {
    $("input[type=radio]").click(function(event){
        var valor = $(event.target).val();
        if(valor =="siI"){
            $("#Otra").show();
        } else if (valor == "noI") {
            $("#Otra").hide();
        } else if(valor=="siRed") {
            $("#divYes").show();
            $("#divNo").hide();
            $("#cell").show();
            $("#esLider").show();
            $("#divYes2").hide();


        } else if(valor=="noRed"){
            $("#divYes").hide();
            $("#cell").hide();
            $("#divNo").show();
            $("#esLider").hide();
            $("#divYes2").hide();
            $("#1").hide();
            $("#11").hide();
            $("#2").hide();
            $("#21").hide();


        }

        else if(valor=="siRed2"){
            $("#divYes2").show();
        }


        if(valor=="noLider"){
            $("#celulaSector").show();
            $("#liderCelula").show();

        }
    });


});

$("#sector_id").change(function () {
    if ($("#sector_id").val()==0) {
        $("#sectorCelula").show();
        $('#AgregarSectores').show();
    }

    if($("#sector_id").val()==-1){
        $("#sectorCelula").hide();
        $('#AgregarSectores').hide();
    }
});

function addSector() {
    //var x = document.getElementById("sector_id");
    //var option = document.createElement("option value='1'");
    var sector = $("#sectorC");
    $('#sector_id').append('<option value='+sector.val()+' selected="selected">'+sector.val()+'</option>');

    //option.text = sector.val();
    //x.add(option);

    $("#sectorCelula").hide();
    $("#AgregarSectores").hide()

}

function mostrarLideres(){
    $("#panelLideres").show();
}

function mostrarPartial(){
    $("#panelCarga").show();
    $("#listaCarga").hide();
}

function mostrarLista(){
    $("#listaCarga").show();
    $("#panelCarga").hide();
}
