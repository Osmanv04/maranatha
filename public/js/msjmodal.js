function MsjEliminar(btn,mensaje){
    $("#alerterror").fadeOut();
    $("#sieliminar").val(btn.value);
    $("#mensajeeliminar").text(mensaje);
}

function MsjExito(mensaje){
    $("#mensajeexito").empty();
    $("#mensajeexito").append(mensaje);
    $("#alertsuccess").fadeIn();
}