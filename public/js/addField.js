$(document).ready(function() {

  var MaxInputs       = 8; //Número Maximo de Campos
  var contenedor       = $("#contenedor"); //ID del contenedor
  var AddButton       = $("#agregarCampo"); //ID del Botón Agrega
  var div = $("#carga") ;
  var quitButton = $("#deleteField");

  //var x = número de campos existentes en el contenedor
  var x = $("#contenedor div").length + 1;
  var FieldCount = x-1; //para el seguimiento de los campos
  var campo;

  $(AddButton).click(function (e) {
      if(x <= MaxInputs) //max input box allowed
      {
          campo = '<div id="carga" class="row"><div  class="form-group col-xs-4"><input type="text" name="nombrep[]" class = "form-control" id="campo_ '+ FieldCount +'" placeholder="Ingrese el nombre "/></div>' +
              '<div class = "form-group col-xs-4"><input type="text" name="apellidop[]" class = "form-control " id="campo_'+ FieldCount +'" placeholder="Ingrese el apellido "/></div>' +
              '<div class = "form-group col-xs-4"><select name="parentesco[]" id="persona_id" class="form-control">' +
              '<option value="Padre">Padre</option>' +
              '<option value="Madre">Madre</option>' +
              '<option value="Hermano(a)">Hermano(a)</option>'+
              '<option value="Esposo(a)">Esposo(a)</option>' +
              '<option value="Hijo(a)">Hijo(a)</option>' +
              '<option value="Otro">Otro</option> </select></div></div> ';
          FieldCount++;
          //agregar campo
          $(contenedor).append(campo);

          console.log(x);
          x++; //text box increment
      }
      return false;
  });


    $(document).on("click",".eliminar",function(){
        $('#carga').remove();
        x = x-1;
        console.log(x);
    });

});
