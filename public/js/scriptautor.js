
$(document).on('click','.pagination a',function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var route = "./autor";

    $.ajax({
        url: route,
        data: {page: page},
        type: 'GET',
        dataType: 'json',
        success: function(data){
            $(".autores").html(data);
            console.log(data);
        }
    });
});

function Eliminar(btn)
{
    var route="./autor/"+btn.value+"";
    var token=$("#token").val();

    $.ajax({
        url:route,
        headers:{'X-CSRF-TOKEN':token},
        type:'DELETE',
        dataType: 'json',
        success: function(msj){
            MsjExito(msj.mensaje);
            $('#fila'+btn.value).fadeOut(1000);
        },
        error:function(){
            $("#mensajeerror").empty();
            alert("No se puede eliminar este autor");

            $("#alerterror").fadeIn(2000);
        }
    });
}

function Mostrar(btn){

    $("#alerterror").fadeOut();

    var route="./autor/"+btn.value+"/edit";

    $.get(route,function(res){
        $("#nombre").val(res.nombre);
        $("#apellido").val(res.apellido);
        $("#id").val(res.id);
    });
}

$("#actualizar").click(function(){
    var value=$("#id").val();
    var nombre=$("#nombre").val();
    var apellido=$("#apellido").val();
    var route="./autor/"+value+"";
    var token=$("#token").val();

    $.ajax({
        url:route,
        headers:{'X-CSRF-TOKEN':token},
        type:'PUT',
        dataType: 'json',
        data:{nombre:nombre, apellido:apellido},
        success: function(msj){

            $("#fila"+value).hide();
            $("#nom"+value).text (nombre);
            $("#ape"+value).text (apellido);
            $("#fila"+value).fadeIn(1000);
            $("#editModal").modal('toggle');

            MsjExito(msj.mensaje);
        },
        error:function(msj){

            $("#mensajeerror").empty();

            $.each( msj.responseJSON, function( key, value ) {
                $("#mensajeerror").append(value);
                $("#mensajeerror").append("<br>");
            });

            $("#alerterror").fadeIn(2000);
        }

    });
});