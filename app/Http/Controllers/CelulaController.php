<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Http\Requests\CelulaCreateRequest;
use Maranatha\Models\Persona\Celula;
use Maranatha\Models\Persona\Persona;
use Maranatha\Models\Persona\Redes;
use Maranatha\Models\Persona\Sector;
use Session;

class CelulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dato = $request->red;
        $celulas = Celula::union($request->red)->paginate(8);
        $lideres = Persona::lideresNo();
        //dd($lideres);
        return view('Celula.index')->with('celulas',$celulas)
                                    ->with('lideres',$lideres)
                                    ->with('dato',$dato);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $redes = Redes::pluck('nombre','id');
        $sectores = Sector::all();
        //dd($redes);
        return view('Celula.create')->with('redes',$redes)
                                    ->with('sectores',$sectores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CelulaCreateRequest $request)
    {
        //dd($request['sector_id']);
        if ($request['sectorC']!='') {
          $sector = Sector::create([
              'nombreSector'=>$request['sectorC']
          ]);
          $sector->save();
          //dd($sector->id);
          $sector_id=$sector->id;
        }

        else {
          $sector_id = $request['sector_id'];
        }
        $cell = Celula::all()->last();
        if($cell ==null) $cont=1;
        else{$cont=$cell->id+1;}
        //dd($cont);
        $celula = Celula::create([
            'name'=>'Maranatha-CS'.$cont,
            'sector_id'=>$sector_id,
            'tipo'=>$request['tipoC'],
            'red_id'=>$request['red_idCelula']
        ]);


        $celula->save();

        Session::flash('save','La celula '.$celula->name.' se la registrado satisfactoriamente');
        return redirect()->route('celulas.index');
    }

    public function registrar(CelulaCreateRequest $request,$id){
        $lider = Persona::find($id);
        //dd($request['red_id']);
        dd($request['red_idCelula']);
        $sector = Sector::create([
            'nombreSector'=>$request['sector_id']
        ]);
        $sector->save();

        $cont = Celula::all()->last();
        $celula = Celula::create([
            'name'=>'Maranatha-CS'.($cont+1),
            'sector_id'=>$sector->id,
            'tipo'=>$request['tipoC'],
            'red_id'=>$request['red_idCelula']
        ]);

        $celula->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $cell = Celula::find($id);
      $sectores = Sector::all();
      $redes = Redes::all();
      return view('Celula.show',compact('cell','sectores','redes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cell = Celula::find($id);
        $sectores = Sector::all();
        $redes = Redes::all();
        return view('Celula.edit',compact('cell','sectores','redes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cell = Celula::find($id);
        $cell->tipo = $request['tipoC'];
        $cell->red_id =$request['red_idCelula'];
        $cell->sector_id = $request['sector_id'];
        $cell->save();

        Session::flash('edit','La celula '.$cell->name.' se ha editado satisfactoriamente');
        return redirect()->route('celulas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cell = Celula::find($id);

        //dd($cell);
        $cell->delete();
        Session::flash('delete','Se ha eliminado satisfactoriamente');
        return redirect()->route('celulas.index');
    }
}
