<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Http\Requests\PastorRequest;
use Maranatha\Models\Persona\Pastores;
use Session;

class PastoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function crear($id){
        //dd($id);
        return view ('pastor.create',compact('id',$id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {


            $nombre = $request['nombrepastor'];
            $apellido = $request['apellidopastor'];
            //dd($request->all());

            for($i=0;$i<count($nombre)and$i<count($apellido);$i++) {
                $pastores = Pastores::create([
                    'nombre' => $nombre[$i],
                    'apellido' => $apellido[$i],

                    'red_id' => $id
                ]);
                $pastores->save();
            }


    }

    public function registrar(PastorRequest $request){
        $pastor=Pastores::create($request->all());
        Session::flash('save','Se ha guardado exitosamente');

        return redirect('redes/'.$pastor->red_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pastor = Pastores::find($id);
        //dd($pastor);
        return view('pastor.show',compact('pastor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pastor = Pastores::find($id);
        //dd($pastor);
        return view('pastor.edit',compact('pastor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PastorRequest $request, $id)
    {
        $pastor = Pastores::find($id);
        $red_id = $pastor->red_id;
        //dd($red_id);
        $input = $request->all();
        $pastor->fill($input)->save();

        return redirect('redes/'.$red_id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pastor = Pastores::find($id);
        $red_id = $pastor->red_id;;


        $pastor->delete();

        Session::flash('delete','Se ha eliminado satisfactoriamente');
        return redirect('redes/'.$red_id.'/edit');
    }
}
