<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Maranatha\Http\Requests;
use Maranatha\Http\Controllers\Controller;
use Maranatha\Models\Persona\Celula;
use Maranatha\Models\Persona\Pastores;
use Maranatha\Models\Persona\Persona;
use Maranatha\Models\Persona\CargaFamiliar;
use Maranatha\Models\Persona\PublicService;
use Maranatha\Models\Persona\Redes;

//use Maranatha\Http\Controllers\App;

class PDFController extends Controller
{
    public function mostrarPersona($id)
    {
        $personas = Persona::mostrarDatos($id);
        $cargasF = CargaFamiliar::mostrarCarga($id);
        $vista = view('persona.personaPDF')
                    ->with(compact('personas'))
                    ->with(compact('cargasF'))
                    ->render();


        $pdf= App::make('dompdf.wrapper');
            //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('persona.pdf');

      //dd($persona);
    }

    public function mostrarListadopersonaF($dato){
        $personas = Persona::personas($dato)->get();
        //dd($personas);

        $vista = view('persona.listadopersonas')
            ->with(compact('personas'))
            ->render();

        /*



        */
        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);
          return $pdf->download('listadopersona.pdf');
    }

    public function mostrarListadopersona(){
        $personas = Persona::personas('')->get();

        $vista = view('persona.listadopersonas')
            ->with(compact('personas'))
            ->render();


        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadopersona.pdf');
    }

    public function mostrarListadoserviciosF($dato){
        //dd($dato);
        $public_services = PublicService::Busqueda($dato)->get();
        //dd($public_services);
        $vista = view('public_service.listadoservicios')
            ->with(compact('public_services'));


        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadoservicios.pdf');
    }

    public function mostrarListadoservicios(){
        $public_services = PublicService::Busqueda('')->get();
        //dd($public_services);
        $vista = view('public_service.listadoservicios')
            ->with(compact('public_services'))
            ->render();


        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadoservicios.pdf');
    }

    public function mostrarListadoredesF($dato){
        $redes = Redes::Busqueda($dato)->get();
        $pastores = Pastores::all();

        $vista = view('redes.listadoredes')
            ->with(compact('redes'))
            ->with(compact('pastores'))
            ->render();


        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadoredes.pdf');
    }

    public function mostrarListadoredes(){
        $redes = Redes::Busqueda('')->get();
        $pastores = Pastores::all();

        $vista = view('redes.listadoredes')
            ->with(compact('redes'))
            ->with(compact('pastores'))
            ->render();



        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadoredes.pdf');
    }

    public function mostrarListadocelulasF($dato){
        $celulas = Celula::union($dato)->get();

        $lideres = Persona::lideresNo();

        $vista = view('celula.listadocelulas')
            ->with(compact('celulas'))
            ->with(compact('lideres'))
            ->render();


        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadocelulas.pdf');
    }

    public function mostrarListadocelulas(){
        $celulas = Celula::union('')->get();

        $lideres = Persona::lideresNo();

        $vista = view('celula.listadocelulas')
            ->with(compact('celulas'))
            ->with(compact('lideres'))
            ->render();



        $pdf= App::make('dompdf.wrapper');
        //App::make
        $pdf->loadHTML($vista);

        return $pdf->download('listadocelulas.pdf');
    }
}
