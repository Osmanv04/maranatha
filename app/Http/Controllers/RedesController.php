<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Http\Requests\RedUpdateRequest;
use Maranatha\Models\Persona\Celula;
use Maranatha\Models\Persona\Redes;
use Maranatha\Models\Persona\Pastores;
use Illuminate\Pagination\Paginator;
use Session;
class RedesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dato=$request->red;
        $redes = Redes::Busqueda($request->red)->paginate(8);
        //dd($redes);
        //$redes = Redes::paginate(5);
        $pastores  = Pastores::get();

        return view('redes.index')->with('redes',$redes)
                                  ->with('pastores',$pastores)
                                    ->with('dato',$dato);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('redes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validar(Request $request){
        $validador = validator($request->all(),[
            'nombre'=>'required|string|min:2|unique:red,nombre',
            'nombrepastor.*'=>'required|string|min:2',
            'apellidopastor.*'=>'required|string|min:2',

        ]);
        return $validador;
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $validador=$this->validar($request);
        //dd($validador->fails());
        if($validador->fails()){
            return redirect('redes/create')->withErrors($validador);
        }
        $pastor = new PastoresController();

        $red = Redes::create([
            'nombre' => $request->nombre,

        ]);

        $red->save();

        $pastores=$pastor->store($request,$red->id);
        Session::flash('save','Se ha guardado exitosamente');

        return redirect()->route('redes.index');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $red = Redes::find($id);

        return view('redes.show',compact('red'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $redes = Redes::find($id);

        $pastores = Pastores::Busqueda($redes->id);
        //dd($redes->id);
        return view('redes.edit',compact('redes',$redes,'pastores',$pastores));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RedUpdateRequest $request, $id)
    {
        $redes= Redes::find($id);

        $redes->nombre = $request['nombre'];
        $redes->save();

        Session::flash('edit','Se ha guardado exitosamente');

        return redirect()->route('redes.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $red = Redes::find($id);
        $pastores = Pastores::eliminarPastor($red->id);

        $red->delete();

        Session::flash('delete','Se ha eliminado satisfactoriamente');
        return redirect()->route('redes.index');
    }


    }
