<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Maranatha\Http\Requests;
use Maranatha\Http\Requests\PersonaUpdateRequest;
use Maranatha\Models\Persona\CargaFamiliar;
use Maranatha\Models\Persona\Celula;
use Maranatha\Models\Persona\Direccion;
use Maranatha\Models\Persona\EdoCivil;
use Maranatha\Models\Persona\Estado;
use Maranatha\Models\Persona\Lider;
use Maranatha\Models\Persona\Persona;
use Maranatha\Models\Persona\Redes;

use Maranatha\Http\Controllers\Controller;
use Maranatha\Models\Persona\Telefono;
use Maranatha\Models\Persona\Sector;
use Maranatha\Http\Requests\PersonaCreateRequest;
use Session;


class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dato = $request->dato;
        //dd($dato);
      $personas=Persona::personas($dato)->paginate(8);
      //dd($personas);

        return view('Persona.index')->with('personas',$personas)
                                            ->with('dato',$dato);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $redes = Redes::pluck('nombre','id');

        $celulas  = Celula::pluck('name','id');
        $edociviles = EdoCivil::all();
        $nombres = Estado::all();
        $sectores = Sector::all();
        //dd(Persona::mostrarDatos(1));

        return view('persona.create')->with('redes', $redes)
                                      ->with('edociviles',$edociviles)
                                      ->with('nombres',$nombres)
                                      ->with('celulas',$celulas)
                                      ->with('sectores',$sectores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validador=$this->validar($request);
        //dd($validador->fails());
        if($validador->fails()){
            return redirect('persona/create')->withErrors($validador);
        }
        $direccion = new DireccionController();
        $telefono = new TelefonoController();
        $carga = new CargaFController();

        $lider_status = false;
        //dd($request['sector']['0']);
        //$celula_id=$celula->store($request);



        $direccion_id = $direccion->store($request);
        $telefono_id=$telefono->store($request);

        $status_red = true;
        $bautizo = true;

        if($request['opcion']=='si'){
            $bautizo= true;
        }
        else {$bautizo=false;}

        if($request['opcion3'] == 'siRed'){
          $status_red = true;
          $red_id = $request['red_id'];
          $celula_id = $request['celula_id'];
        }
        elseif ($request['opcion4']=='siRed2') {
          $status_red = true;
          $red_id = $request['red_id2'];
          $celula_id = null;
        }

        else {
          $status_red = false;
          $red_id = null;
          $celula_id = null;
        }

        if($request['opcion5']=='siLider'){
            $lider_status = true;
        }

        else $lider_status = false;

        if ($request['celula_id']=='') {
          $celula_id=null;
        }
        //dd($request['red_id']);
        $persona = Persona::create([
            'red_id'=> $red_id,
            'nombre' =>$request['nombre'],
            'apellido'=>$request['apellido'],
            'cedula' => $request['cedula'],
            'correo'=>$request['correo'],
            'otraIglesia'=>$request['iglesia'],
            'fecha_nac'=>$request['fecha_nac'],
            'ocupacion' =>$request['ocupacion'],
            'bautizo'=>$bautizo,
            'tiempoIglesia'=>$request['tiempo'],
            'celula_id'=>$celula_id,
            'status_red'=>$status_red,
            'direccion_id'=>$direccion_id,
            'edocivil_id'=>$request['edocivil_id'],
            'tlf_id'=>$telefono_id,
            'esLider'=>$lider_status
        ]);
        $persona->save();
        $cargaF = $carga->store($request,$persona->id);
        Session::flash('save','Se ha guardado exitosamente');

        return redirect()->route('persona.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $persona = Persona::find($id);

        return view('persona.show')->with('persona',$persona);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $redes = Redes::all();
        $edociviles = EdoCivil::all();
        $nombres = Estado::all();
        $celulas= Celula::all();
        $persona = Persona::find($id);
        $sectores = Sector::all();
        //dd(date("d-m-Y",strtotime($persona->fecha_nac)));
        $direccion = Direccion::find($persona->direccion_id);
        $cargasF = CargaFamiliar::mostrarCarga($persona->id);
        $tlf = Telefono::find($persona->tlf_id);
        $ban = 0;
        //dd($persona->tlf_id);
        return view('persona.edit',array('persona'=>$persona,'redes'=>$redes,
            'nombres'=>$nombres,'edociviles'=>$edociviles,'direccion'=>$direccion,
            'cargasF'=>$cargasF,'tlf'=>$tlf,'celulas'=>$celulas,'sectores'=>$sectores));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonaUpdateRequest $request, $id)
    {
        //dd($request->all());
        $personas= Persona::find($id);
        $status_red =false;
        if ($request['sectorC']!='') {
            $sector = Sector::create([
                'nombreSector'=>$request['sectorC']
            ]);
            $sector->save();
            $sector_id=$sector->id;
        }

        else {
            $sector_id = $request['sector_id'];
        }
        $direccion = Direccion::find($personas->direccion_id);

            $direccion->municipio = $request['municipio'];
            $direccion->estado_id =$request['estado_id'];
            $direccion->avenida =$request['avenida'];
            $direccion->calle =$request['calle'];
            $direccion->sector_id =$sector_id;
            $direccion->nroCasa =$request['nroCasa'];
            $direccion->edificio =$request['edificio'];

            $direccion->save();

            $telefono = Telefono::find($personas->tlf_id);

            $telefono->movil = $request['movil'];
            $telefono->casa = $request['casa'];
            $telefono->save();

            $personas->nombre = $request['nombre'];
            $personas->apellido=$request['apellido'];
            $personas->cedula= $request['cedula'];
            $personas->red_id= $request['red_id'];
            $personas->correo=$request['correo'];
            $personas->status_Red=$request['status_Red'];
            $personas->fecha_nac=$request['fecha_nac'];
            $personas->ocupacion =$request['ocupacion'];
            $personas->direccion_id=$direccion->id;
            $personas->edocivil_id=$request['edocivil_id'];
            $personas->celula_id = $request['celula_id'];
        if($request['opcion']=='si'){
            $bautizo= true;
            $personas->bautizo=$bautizo;
        }
        else {$bautizo=false;
            $personas->bautizo=$bautizo;
        }

        if($request['opcion3'] == 'siRed'){
            $status_red = true;
            $red_id = $request['red_id'];
            $celula_id = $request['celula_id'];
            $personas->status_red=$status_red;
            $personas->red_id=$red_id;
            $personas->celula_id=$celula_id;
        }
        elseif ($request['opcion4']=='siRed2') {
            $status_red = true;
            $red_id = $request['red_id2'];

            $celula_id = null;
            $personas->status_red=$status_red;
            $personas->red_id=$red_id;
            $personas->celula_id=$celula_id;
        }
            if($request['opcion5']=='siLider'){
                $lider_status = true;
                $personas->esLider=$lider_status;
            }

            else {$lider_status = false;
                $personas->esLider=$lider_status;
            }
            $personas->tlf_id=$telefono->id;

            //dd($status_red);

        $personas->save();

        Session::flash('edit','Se ha editado exitosamente');

        return redirect()->route('persona.index');

    }

    public function validar($request){
        $validador = validator($request->all(),[
            'nombre'=>'required|string|min:2',
            'apellido'=>'required|string|min:2',
            'cedula'=>'required|numeric|unique:persona,cedula',
            'fecha_nac'=>'required',
            'nombrep.*'=>'required',
            'apellidop.*'=>'required',
            'parentesco.*'=>'required',
            'correo'=>'required|email',
            'ocupacion'=>'required',
            'edocivil_id'=>'required',
            'estado_id' => 'required',
            'municipio' => 'required',
            'sector_id' => 'required',
            'calle' => 'required',
            'tiempo' => 'required',
            'movil'=>'required|digits:11',
            'casa'=>'digits:11',
        ]);
        return $validador;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $persona = Persona::find($id);
        $cargasF = CargaFamiliar::eliminarCarga($persona->id);

        $persona->delete();

        Session::flash('delete','Se ha eliminado satisfactoriamente');
        return redirect()->route('persona.index');
    }


  public function mostrar($id) {
    $personas = Persona::mostrarDatos($id);
    $cargasF = CargaFamiliar::mostrarCarga($id);

    //dd($cargasF);

    //($personas['0']->id);
    return view('persona.mostrar',compact('personas','cargasF'));
  }
}
