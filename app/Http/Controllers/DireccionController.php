<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Models\Persona\Direccion;
use Maranatha\Models\Persona\Sector;

class DireccionController extends Controller
{
    public function store(Request $request){
      //dd($request['sector_id']);
      if ($request['sectorC']!='') {
        $sector = Sector::create([
            'nombreSector'=>$request['sectorC']
        ]);
        $sector->save();
        $sector_id=$sector->id;
      }

      else {
        $sector_id = $request['sector_id'];
      }


        $direccion = Direccion::create([
            'municipio' => $request['municipio'],
            'estado_id' =>$request['estado_id'],
            'avenida' =>$request['avenida'],
            'calle' =>$request['calle'],
            'nroCasa' =>$request['nroCasa'],
            'edificio' =>$request['edificio'],
            'sector_id'=>$sector_id
        ]);
        $direccion->save();

        return $direccion->id;
    }
}
