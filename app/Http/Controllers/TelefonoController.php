<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Models\Persona\Telefono;

class TelefonoController extends Controller
{
    public function store(Request $request){
        $telefono = Telefono::create([
            'movil'=>$request['movil'],
            'casa'=>$request['casa']
        ]);
        $telefono->save();
        return $telefono->id;
    }
}
