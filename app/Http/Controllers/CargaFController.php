<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Http\Requests\CargaFRequest;
use Maranatha\Http\Requests\PersonaCreateRequest;
use Maranatha\Models\Persona\CargaFamiliar;
use Session;

class CargaFController extends Controller
{
    public function store(Request $request,$persona_id){
        $nombres = $request['nombrep'];
        $apellidos = $request['apellidop'];
        $parentescos = $request['parentesco'];


        for($i=0;$i<count($nombres)and$i<count($apellidos)and$i<count($parentescos);$i++){
            $carga = CargaFamiliar::create([
                'nombrep'=>$nombres[$i],
                'apellidop'=>$apellidos[$i],
                'parentesco'=>$parentescos[$i],
                'persona_id'=>$persona_id
            ]);
            $carga->save();
        }

    }

    public function show($id)
    {
        $carga = CargaFamiliar::find($id);
        //dd($pastor);
        return view('CargaFamiliar.show',compact('carga'));
    }

    public function registrar(CargaFRequest $request){
        $persona_id=$request['persona_id'];
        //dd($persona_id);
        $cargaf = CargaFamiliar::create($request->all());
        $cargaf->save();
        $ban =1;
        return redirect('persona/'.$persona_id.'/edit')->with($ban);
    }

    public function crear($id){
        return view ('CargaFamiliar.create',compact('id',$id));
    }
/*
    public function crear($id){
        return view('CargaFamiliar.create',compact('id',$id));
      /*dd($request['nombre']);
        if($request->ajax()) {
            $result = CargaFamiliar::create($request->all());
            $result->save();
            if ($result) {
                Session::flash('save', 'Se ha creado correctamente');
                return response()->json(["mensaje" => "creado"]);
            }
        }


      }
*/
    public function edit($id)
    {
        $carga = CargaFamiliar::find($id);

        //dd($pastor);
        return view('CargaFamiliar.edit',compact('carga'));
    }
    

    public function update(CargaFRequest $request, $id)
    {
        $cargaf = CargaFamiliar::find($id);
        $persona_id = $cargaf->persona_id;
        //dd($red_id);
        $input = $request->all();
        $cargaf->fill($input)->save();
        ;

        return redirect('persona/'.$persona_id.'/edit');
    }

    public function destroy($id)
    {
        $cargaf = CargaFamiliar::find($id);
        $persona_id = $cargaf->persona_id;;


        $cargaf->delete();

        Session::flash('delete','Se ha eliminado satisfactoriamente');
        $ban2 = 1;
        return redirect('persona/'.$persona_id.'/edit')->with($ban2);
    }


}
