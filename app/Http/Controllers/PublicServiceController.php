<?php

namespace Maranatha\Http\Controllers;

use Illuminate\Http\Request;

use Maranatha\Http\Requests;
use Maranatha\Http\Requests\ServiceCreateRequest;
use Maranatha\Models\Persona\PublicService;
use Maranatha\Models\Persona\Sector;
use Session;

class PublicServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dato = $request->sector;
        $public_services = PublicService::Busqueda($request->sector)->paginate(8);
        //dd($public_services);
        return view('public_service.index')
            ->with('public_services',$public_services)
            ->with('dato',$dato);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sectores= Sector::all();
        return view('public_service.create',compact('sectores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceCreateRequest $request)
    {
        if ($request['sectorC']!='') {
            $sector = Sector::create([
                'nombreSector'=>$request['sectorC']
            ]);
            $sector->save();
            $sector_id=$sector->id;
        }

        else {
            $sector_id = $request['sector_id'];
        }
        $public_service = PublicService::create([
            'tipo'=>$request['tipo'],
            'encargado'=>$request['encargado'],
            'contacto'=>$request['contacto'],
            'sector_id'=>$sector_id
        ]);
        $public_service->save();
        Session::flash('save','Se ha guardado exitosamente');
        return redirect()->route('public_service.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $public_service = PublicService::find($id);

        return view('public_service.show')->with('public_service',$public_service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sectores = Sector::all();
        $public_service = PublicService::find($id);
        return view('public_service.edit',array('public_service'=>$public_service,'sectores'=>$sectores));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceCreateRequest $request, $id)
    {
        if ($request['sectorC']!='') {
            $sector = Sector::create([
                'nombreSector'=>$request['sectorC']
            ]);
            $sector->save();
            $sector_id=$sector->id;
        }

        else {
            $sector_id = $request['sector_id'];
        }
        $public_service = PublicService::find($id);
        $public_service->tipo = $request['tipo'];
        $public_service->encargado = $request['encargado'];
        $public_service->contacto = $request['contacto'];
        $public_service->sector_id=$sector_id;
        $public_service->save();
        //$input = $request->all();
        //$public_service->fill($input)->save();
        Session::flash('edit','Se ha editado satisfactoriamente');

        return redirect()->route('public_service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $public_service = PublicService::find($id);


        $public_service->delete();

        Session::flash('delete','Se ha eliminado satisfactoriamente');
        return redirect()->route('public_service.index');
    }
}
