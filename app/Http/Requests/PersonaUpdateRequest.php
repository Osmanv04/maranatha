<?php

namespace Maranatha\Http\Requests;

use Maranatha\Http\Requests\Request;
use Illuminate\Routing\Route;

class PersonaUpdateRequest extends Request
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Route $route){

        $this->route = $route;
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'nombre'=>'required|string|min:2',
            'apellido'=>'required|string|min:2',
            'cedula'=>'required|numeric|unique:persona,cedula,'.$this->route->getParameter('persona'),
            'fecha_nac'=>'required|date',
            'correo'=>'required|email',
            'ocupacion'=>'required',
            'edocivil_id'=>'required',
            'estado_id' => 'required',
            'municipio' => 'required',
            'sector_id' => 'required',
            'calle' => 'required',
            'tiempo' => 'required',
            'movil'=>'required|digits:11',
            'casa'=>'digits:11',
        ];
        return $rules;
    }
}
