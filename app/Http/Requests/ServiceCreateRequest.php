<?php

namespace Maranatha\Http\Requests;

use Maranatha\Http\Requests\Request;

class ServiceCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo'=>'required',
            'sector_id'=>'required',
            'encargado'=>'required',
            'contacto'=>'required|digits:11'
        ];
    }
}
