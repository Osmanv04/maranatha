<?php

namespace Maranatha\Http\Requests;

use Maranatha\Http\Requests\Request;

class PersonaCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'nombre'=>'required|string|min:2',
            'apellido'=>'required|string|min:2',
            'cedula'=>'required|integer|unique:persona,cedula',
            'fecha_nac'=>'required|date',

        ];
        //dd($rules['nombrep.'.'0']);
       // dd($rules['nombrep']);
        $parentesco=$this->request->get('parentesco');
        $nombrep = $this->request->get('nombrep');
        $apellidop = $this->request->get('apellidop');
        //$parentesco=$this->request->get('parenstesco');
        //dd($this->request->get('nombrep'));
        for($i=0;$i<count($nombrep);$i++){
           $rules['nombrep.'.$i]='required|max:10';

        }
        //dd($rules);

    return $rules;

    }
}
