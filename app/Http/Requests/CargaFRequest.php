<?php

namespace Maranatha\Http\Requests;

use Maranatha\Http\Requests\Request;

class CargaFRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombrep'=>'required',
            'apellidop'=>'required',
            'parentesco'=>'required'
        ];
    }
}
