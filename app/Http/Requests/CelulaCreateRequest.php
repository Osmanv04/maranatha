<?php

namespace Maranatha\Http\Requests;

use Maranatha\Http\Requests\Request;

class CelulaCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sector_id'=>'required|not_in:0|not_in:-1',
            'tipoC'=>'required|not_in:0',
            'red_idCelula'=>'required'
        ];
    }
}
