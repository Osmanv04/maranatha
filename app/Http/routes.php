<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//ruta para exportar a PDF información de persona
Route::get('PDFpersona/{id}','PDFController@mostrarPersona');

//rutas para exportar a PDF los listados
Route::get('PDFListadopersonaF/{dato}','PDFController@mostrarListadopersonaF');
Route::get('PDFListadopersona','PDFController@mostrarListadopersona');

Route::get('PDFListadoservicios/{dato}','PDFController@mostrarListadoserviciosF');
Route::get('PDFListadoservicios','PDFController@mostrarListadoservicios');

Route::get('PDFListadoredes/{dato}','PDFController@mostrarListadoredesF');
Route::get('PDFListadoredes','PDFController@mostrarListadoredes');

Route::get('PDFListadocelulas/{dato}','PDFController@mostrarListadocelulasF');
Route::get('PDFListadocelulas','PDFController@mostrarListadocelulas');

//Rutas para personas
Route::get('/','PersonaController@index');
Route::get('/persona/mostrar/{id}','PersonaController@mostrar');
Route::resource('/persona','PersonaController');

//Ruta para Carga Familiar
Route::get('/cargaf/crear/{id}','CargaFController@crear');
Route::post('/cargaf/registrar','CargaFController@registrar');
Route::resource('/cargaf','CargaFController');

//Ruta para Celulas
Route::resource('/celulas','CelulaController');

//Ruta para Servicio público
Route::resource('/public_service','PublicServiceController');

//Ruta para Redes
Route::resource('/redes','RedesController');

//Ruta para pastores
Route::get('/pastor/crear/{id}','PastoresController@crear');

Route::post('/pastor/registrar','PastoresController@registrar');

Route::resource('/pastor','PastoresController');
