<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Redes extends Model
{
    protected $table = 'red';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','nombre'
    ];

    public function persona(){
        return $this->belongsTo(Persona::class);
    }

    public function pastor(){
        return $this->belongsTo(Pastores::class);
    }

    public function celulas(){
        return $this->hasMany(Celula::class);
    }

    public static function scopeMostrar(){
        return DB::table('red')
            ->join('pastores as p','red.id','=','p.red_id')
            ->select('p.nombre','p.apellido','p.red_id','red.nombre as red','red.id as id')
            ->paginate(6);

    }


    public static function Busqueda($dato){

        return DB::table('red')->where('nombre','LIKE','%'.$dato.'%');
    }
}
