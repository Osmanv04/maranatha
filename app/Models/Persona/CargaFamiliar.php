<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;
use Maranatha\Models\Persona\Persona;
use Illuminate\Support\Facades\DB;

class CargaFamiliar extends Model
{
    protected $table = 'cargaFamiliar';
    protected $primarykey = 'id';
    protected $fillable=[
        'id','nombrep', 'apellidop','parentesco','persona_id'
    ];

    public function Persona(){
        $this->hasMany(Persona::class);
    }

    public static function mostrarCarga($id){
        return DB::table('cargaFamiliar')
            ->join('persona','persona.id','=','cargaFamiliar.persona_id')
            ->where('persona.id','=',$id)
            ->select('cargaFamiliar.*')
            ->paginate(8);
    }

    public static function eliminarCarga($persona_id){
      return DB::table('cargaFamiliar')

            ->from('cargaFamiliar')
            ->where('cargaFamiliar.persona_id','=',$persona_id)
            ->delete();

    }
}
