<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;


class Direccion extends Model
{
      protected $table = 'direccion';
      protected $primarykey = 'id';
      protected $fillable = [
        'id','municipio','estado_id','avenida','calle','sector_id','nroCasa','edificio'

      ];

    public function persona(){
        return $this->belongsTo(Persona::class);
    }




}
