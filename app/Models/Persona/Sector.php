<?php

namespace Maranatha\Models\Persona;


use Illuminate\Database\Eloquent\Model;
use Maranatha\Models\Persona\Celula;
use Maranatha\Models\Persona\Direccion;

class Sector extends Model
{
    protected $table = 'sectors';
    protected  $primarykey = 'id';
    protected $fillable = ['id','nombreSector'];

    public function direccion(){
        return $this->belongsTo(Direccion::class);
    }

    public function celula(){

        return $this->belongsTo(Celula::class);
    }
}
