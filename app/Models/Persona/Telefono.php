<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'telefono';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','movil','casa'
    ];

    public function Persona(){
      return $this->belongsTo(Persona::class);
    }
}
