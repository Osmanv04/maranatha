<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;

class EdoCivil extends Model
{
    protected $table = 'edocivil';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','estado'
    ];

    public function persona(){
        return $this->belongsTo(Persona::class);
    }
}
