<?php

namespace Maranatha\Models\Persona;

use Faker\Provider\fr_CA\Person;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lider extends Model
{
    protected $table="liders";
    protected $primarykey="id";
    protected $fillable =[
        'id','celula_id','persona_id','tlf_id'
    ];

    public function celula(){
        return $this->belongsTo(Celula::class);
    }

    public function persona(){
        return $this->hasOne(Person::class);
    }


    public static function joinLiders(){
        return DB::table('liders')
            ->join('persona','persona.id','=','liders.id')
            ->select('liders.id','persona.nombre','persona.apellido')
            ->get();
    }

}
