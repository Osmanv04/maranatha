<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



class PublicService extends Model
{
    protected $table = 'public_services';
    protected $primarkey= 'id';
    protected $fillable=[
        'id','tipo','encargado','sector_id','contacto'
    ];

    public static function Busqueda($dato){

        return DB::table('public_services')
            ->join('sectors','sectors.id','=','public_services.sector_id')
            ->where('sectors.nombreSector','LIKE','%'.$dato.'%')
            ->select('public_services.id','public_services.tipo','public_services.encargado','public_services.contacto','sectors.nombreSector as sector');
    }

}
