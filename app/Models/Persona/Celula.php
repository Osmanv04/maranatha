<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Celula extends Model
{
    protected $table = "celulas";
    protected $primarykey = 'id';
    protected $fillable = [
        'id','tipo','sector_id','red_id','name'
    ];

    public function red(){

        return $this->belongsTo(Redes::class);
    }

    public function pastor(){
        return $this->hasOne(Pastores::class);
    }


    public function sector(){
        return $this->hasOne(Sector::class);
    }

    public static function celula($id){
        return Celula::where('red_id','=',$id)
            ->get();
    }

    public static function union($dato){
        return DB::table('celulas')
                                ->join('sectors','sectors.id','=','celulas.sector_id')
                                ->join('red','red.id','=','celulas.red_id')
                                ->where('red.nombre','LIKE','%'.$dato.'%')
                                ->orWhere('sectors.nombreSector','LIKE','%'.$dato.'%')
                                ->orWhere('celulas.tipo','LIKE','%'.$dato.'%')
                                ->select('celulas.id','celulas.name','celulas.tipo','red.nombre as red','sectors.nombreSector as sector');
    }


}
