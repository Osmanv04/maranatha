<?php

namespace Maranatha\Models\Persona;

use Faker\Provider\Person;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Persona extends Model
{
    protected $table = 'persona';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','nombre','apellido','cedula','fecha_nac','direccion_id','ocupacion',
      'tlf_id','bautizo','iglesia_id','correo','edocivil_id','red_id',
        'celula_id','tiempoIglesia','status_red','esLider','otraIglesia'
    ];

    public function red(){
        return $this->hasOne(Red::class);
    }

    public function direccion(){

        return $this->hasOne(Direccion::class);
    }

    public function edoCivil(){

        return $this->hasOne(EdoCivil::class);
    }

    public function CargaF(){
        return $this->belongsTo(CargaFamiliar::class);
    }

    public function telefono(){
        return $this->hasOne(Telefono::class);
    }

    public function lider(){
        return $this->belongsTo(Lider::class);
    }

    public function celula(){
        return $this->hasOne(Celula::class);
    }

    public static function lideresNo(){
        return Persona::where('esLider','=','1')
            //->where('', 'pattern')
            ->orWhere('celula_id','=',null)->get();

    }


    public static function personas($dato)
    {
      return DB::table('persona')
                                ->join('telefono','telefono.id','=','persona.tlf_id')
                                ->leftJoin('red','persona.red_id','=','red.id')
                                ->where('red.nombre','LIKE','%'.$dato.'%')
                                ->orWhere('persona.nombre','LIKE','%'.$dato.'%')
                                ->orWhere('persona.apellido','LIKE','%'.$dato.'%')
                                ->select('persona.id','persona.nombre as persona','persona.apellido',
                                           'telefono.movil as telefono','red.nombre as red');
    }

    public static function mostrarDatos($id){
        return DB::table('persona')
                                ->join('telefono','telefono.id','=','persona.tlf_id')
                                ->leftJoin('red','persona.red_id','=','red.id')
                                ->leftJoin('celulas','persona.celula_id','=','celulas.id')
                                ->join('direccion','persona.direccion_id','=','direccion.id')
                                ->join('edocivil','persona.edocivil_id','=','edocivil.id')
                                ->join('estado','estado.id','=','direccion.estado_id')
                                ->join('sectors','sectors.id','=','direccion.sector_id')
                                ->where('persona.id', $id)
                                //->where('persona.red_id','NULL')
                                ->select('persona.id','persona.nombre','persona.apellido',
                                    'telefono.movil as telefono','telefono.casa as casa','red.nombre as red','persona.fecha_nac','persona.red_id','persona.celula_id',
                                    'edocivil.estado as edocivil','persona.ocupacion','celulas.name as celula','estado.nombre as estado',
                                    'persona.cedula','persona.correo','persona.tiempoIglesia','persona.otraIglesia','persona.esLider','persona.tlf_id',
                                    'persona.bautizo','persona.status_red','direccion.municipio','direccion.avenida',
                                    'direccion.calle','direccion.nroCasa','direccion.edificio','sectors.nombreSector')
                                ->get();
    }


}
