<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pastores extends Model
{
    protected $table = 'pastores';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','nombre','apellido','tlf_id','red_id','celula_id'
    ];

    public function red(){
        return $this->hasOne(Redes::class);
    }

    public function celula(){

        return $this->hasOne(Celula::class);
    }

    public static function Busqueda($id){
        return DB::table('pastores')
        ->join('red','red.id','=','pastores.red_id')
            ->where('red.id','=',$id)
            ->select('pastores.*')
            ->get();
    }

    public static function eliminarPastor($id){
        return DB::table('pastores')

            ->from('pastores')
            ->where('pastores.red_id','=',$id)
            ->delete();

    }

}
