<?php

namespace Maranatha\Models\Persona;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estado';
    protected $primarykey = 'id';
    protected $fillable = [
      'id','nombre'
    ];
}
