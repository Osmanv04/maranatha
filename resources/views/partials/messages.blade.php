@if (count($errors)>0)
  <div id="alert-danger" class="alert alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Errores:</strong>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
@endif
@if(Session::has('save'))
    <div class="alert alert-success" role = 'alert'>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{Session::get('save')}}</strong>
    </div>
    @endif

    @if(Session::has('edit'))

        <div class="alert alert-success" role = 'alert'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{Session::get('edit')}}</strong>
        </div>
        @endif

    @if(Session::has('delete'))

        <div class="alert alert-danger" role = 'alert'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{Session::get('delete')}}</strong>
        </div>
        @endif
