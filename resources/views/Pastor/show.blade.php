@extends('layouts.master')

@section('title')

@section('content')
{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            {!! Form::open(['id'=>'idForm','route' =>['pastor.destroy',$pastor->id],'method'=>'DELETE']) !!}
            <div class="form-group">
                <label for="exampleInputPassword1">Desea borrar a este pastor:</label>
            </div>
            <div class="form-group">
                <label>Pastor: </label>
                {{$pastor->nombre}} {{$pastor->apellido}}
            </div>

            {!!Form::submit('Eliminar',['name'=>'delete','id'=>'delete','content'=>'<span>Eliminar</span>',
            'class'=>'btn btn-danger btn-sm m-t-10'])!!}

            <a type="button" name="cancelar" href="{{route('redes.edit',$pastor->red_id)}}" class="btn btn-default btn-sm m-t-10">Cancelar</a>

            {!! Form::close() !!}
        </div>
    </div>
</div>



{!!Html::script('js/jquery-1.12.2.js')!!}
{!!Html::script('js/bootstrap.min.js')!!}
{!!Html::script('js/step.js')!!}
{!!Html::script('js/addField.js')!!}

@endsection