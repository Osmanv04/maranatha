@extends('layouts.master')
@section('title')
@section('content')
@include('partials.messages')

{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span>Editar Pastor</span>
                </div>

                <div class="panel-body">

                    {!! Form::model($pastor,['id'=>'idFormEdit','route' =>['pastor.update',$pastor->id],'method'=>'PUT']) !!}


                    <div class="form-group">
                        {!! Form::label('Nombre del pastor') !!}
                        <input type="text" name = "nombre" id="nombre" class="form-control"placeholder="Ingrese el nombre de la red" value="{{$pastor['nombre']}}">

                    </div>

                    <div class="form-group">
                        {!! Form::label('Apellido del pastor') !!}
                        <input type="text" name = "apellido" id="nombre" class="form-control"placeholder="Ingrese el nombre de la red" value="{{$pastor['apellido']}}">

                    </div>


                </div>
                <div class="modal-footer">

                    <button class="btn btn-success btn-md pull-right" type="submit">Editar</button>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    {!!Html::script('js/step.js')!!}
    {!!Html::script('js/addField.js')!!}
@endsection