@extends('layouts.master')
@section('title')
@section('content')
@include('partials.messages')

{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span>Agregar Pastor</span>
                </div>

                <div class="panel-body">

                    {!! Form::open(['id'=>'idModalPastor','url'=>'pastor/registrar','method'=>'POST']) !!}


                    <div class="form-group">
                        {!! Form::label('Nombre del pastor') !!}
                        {!! Form::text('nombrepastor',null,['id'=>'nombrep','class'=>'form-control','placeholder'=>'Ingrese el nombre del pastor']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('Apellido del pastor') !!}
                        {!! Form::text('apellidopastor',null,['id'=>'apellido','class'=>'form-control','placeholder'=>'Ingrese el apellido del pastor']) !!}

                    </div>

                    <input type="hidden" name="red_id" value="{{$id}}">


                </div>
                <div class="modal-footer">

                    <button class="btn btn-success btn-md pull-right" type="submit">Registrar</button>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    {!!Html::script('js/step.js')!!}
    {!!Html::script('js/addField.js')!!}
@endsection

