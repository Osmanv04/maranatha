<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/metisMenu.min.css')!!}
    {!!Html::style('css/sb-admin-2.css')!!}
    {!!Html::style('css/font-awesome.min.css')!!}
    {!!Html::style('css/centro.css')!!}

</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">

            <a class="navbar-brand "  href="{!! URL::to('/persona') !!}"> <img src="{!! URL::to('/img/logo.png') !!}" style="margin-top:-50px;" align="center" alt=""/> </a>
        </div>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{!!URL::to('/celulas')!!}"><i class="fa fa-random" ></i> Celula<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/celulas/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/celulas')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>
                    <li>

                        <a href="#"><i class="fa fa-male fa-fw"></i>Integrantes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/persona/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/persona')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{!!URL::to('/redes')!!}"><i class="fa fa-code-fork fa-fw"></i> Redes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/redes/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/redes')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{!!URL::to('/public_service')!!}"><i class="fa fa-ambulance fa-fw"></i> Servicios Publicos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/public_service/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/public_service')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>

    </nav>

    <div id="page-wrapper">
        @yield('content')
    </div>
    <div class="footer">
        <div class="text-center">
            <strong>Realizado por:</strong><br>
                <strong>Ing. Osman Villegas</strong>
        </div>
    </div>

</div>


{!!Html::script('js/jquery.min.js')!!}
{!!Html::script('js/bootstrap.min.js')!!}
{!!Html::script('js/metisMenu.min.js')!!}
{!!Html::script('js/sb-admin-2.js')!!}


@yield('scripts')


</body>

</html>
