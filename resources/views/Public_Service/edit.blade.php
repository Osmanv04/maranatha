@extends('layouts.master')

@section('title')

@section('content')
    {!!Html::style('css/example.css')!!}
    <!-- Main component for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                @include('partials.messages')

                {!! Form::model($public_service,['id'=>'idForm','route' =>['public_service.update',$public_service->id],'method'=>'PUT']) !!}

                <div class="col-xs-6 ">
                    <div class="form-group">
                        {!!Form::label('Tipo de servicio')!!}
                        {!!Form::text('tipo',null,['id'=>'tipo','class'=>'form-control','placeholder'=>'Ingrese el servicio'])!!}
                    </div>
                </div>
                <div class="form-group col-xs-6" >

                    {!! Form::label('Sector') !!}
                    <div class="row">
                        <div id="divSectores" class="col-xs-5">
                            <select name="sector_id" id="sector_id" class="form-control">
                                <option value="-1"></option>
                                <option value="0">Agregar Sector</option>
                                @foreach($sectores as $sector)
                                    @if($sector->id==$public_service->sector_id)
                                        <option selected="selected" value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                                    @else
                                        <option value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div id="sectorCelula" class="col-xs-4" style="display:none;">
                            <input id="sectorC" name="sectorC" type="text"  class="form-control" placeholder="Ingrese el sector a agregar">
                        </div>
                        <div id="AgregarSectores" class="col-xs-1" style="display: none;">
                            <button id="agregarSector" type="button" onclick="addSector()" class="btn btn-default btn-sm" title="Agregar Sector">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </div>

                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">

                        {!! Form::label('Encargado') !!}
                        {!! Form::text('encargado',null,['id'=>'encargado','class'=>'form-control','placeholder'=>'Encargado']) !!}

                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">

                        {!! Form::label('Número de contacto') !!}
                        {!! Form::text('contacto',null,['id'=>'contacto','class'=>'form-control']) !!}

                    </div>
                </div>
                <div class="col-xs-6 col-md-offset-6">
                    <button class="btn btn-success btn-lg pull-right" type="submit">Editar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    {!! Html::script('js/showDivs.js') !!}
@endsection
