@extends('layouts.master')
@section('title')
@section('content')
    @include('partials.messages')

    {!!Html::style('css/example.css')!!}
    <!-- Main component for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="col-md-10">

                {!! Form::open(['route'=>'public_service.index','method'=>'GET']) !!}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="col-xs-8">
                            <button class="btn btn-info btn-flat" type="submit" title="Todos los servicios"  >
                                <span class="glyphicon glyphicon-home"></span> Lista Completa
                            </button>

                            @if($dato==null)
                                <a href="{{URL::to('PDFListadoservicios')}}" class="btn btn-success" type="button">Exportar PDF</a>
                            @else
                                <a href="{{URL::to('PDFListadoservicios',$dato)}}" class="btn btn-success" type="button">Exportar PDF</a>
                            @endif
                        </div>

                        <div class="input-group input-group-sm col-xs-offset-4">
                            <input type="text" name="sector" class="form-control" placeholder="Ingrese sector" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-info btn-flat" type="submit" title="Buscar Servicio"  >
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                    </div>

                    <div class="panel-body">

                        <table class="table table-bordered">
                            <thead>
                            <th class="col-md-2">Tipo</th>
                            <th class="col-md-3">Encargado</th>
                            <th class="col-md-3">Sector</th>
                            <th class="col-md-2">Contacto</th>
                            <th class="col-md-2">Accion</th>

                            </thead>
                            <tbody>
                            @foreach($public_services as $public_service)
                                <tr>
                                    <td>{{$public_service->tipo}}</td>
                                    <td>{{$public_service->encargado}}</td>

                                    <td>{{$public_service->sector}}</td>
                                    <td>{{$public_service->contacto}}</td>
                                    <td> <a type="button" href="{{route('public_service.edit',$public_service->id)}}" title="Editar" class='btn btn-social-icon btn-info' >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a type="button" href="{{route('public_service.show',$public_service->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                            <i class="fa fa-trash"></i>
                                        </a>

                                        <button title="Informacion" class="btn btn-social-icon btn-info">
                                            <i class="fa fa-info"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>


                        </table>

                        <div class="text-center">
                            {!! $public_services->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
