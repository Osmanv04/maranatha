@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}
   <!-- Main component for a primary marketing message or call to action -->
<div class="container">
  <div class="row">
    <div class="col-md-10">

  {!! Form::open(['id'=>'idForm','route' =>['public_service.destroy',$public_service->id],'method'=>'DELETE']) !!}
    <div class="form-group">
      <label for="exampleInputPassword1">Desea eliminar este servicio:</label>
    </div>
    <div class="form-group">
      <label>Servicio</label>
      {{$public_service->tipo}}
    </div>

    {!!Form::submit('Eliminar',['name'=>'delete','id'=>'delete','content'=>'<span>Eliminar</span>',
    'class'=>'btn btn-danger btn-sm m-t-10'])!!}

    <a type="button" name="cancelar" href="{{route('public_service.index')}}" class="btn btn-default btn-sm m-t-10">Cancelar</a>

        {!! Form::close() !!}
</div>
</div>
    </div>


@endsection
