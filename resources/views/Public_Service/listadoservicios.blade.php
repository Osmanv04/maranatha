<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Listado de Servicios</title>
    <style>
        table, td, th {
            border: 1px solid #ddd;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 15px;
        }
        h3{
            text-align: center;
        }
        #page-wrap { padding: 80px; }
    </style>

</head>

<body>
<div id="page-wrap">
    <img src="./img/logo.png" style="margin-top:-50px;" aling="center">
    <h3>Listado de Servicios Públicos</h3>
    <table >

            <tbody>
            <tr>
                <th >Tipo</th>
                <th >Encargado</th>
                <th >Sector</th>
                <th >Contacto</th>

            </tr>
            @foreach($public_services as $public_service)
                <tr>
                    <td>{{$public_service->tipo}}</td>
                    <td>{{$public_service->encargado}}</td>

                    <td>{{$public_service->sector}}</td>
                    <td>{{$public_service->contacto}}</td>

                </tr>
            @endforeach

            </tbody>


        </table>


</div>
</body>
</html>