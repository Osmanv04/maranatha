@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}

<div class="container">
  <div class="row">
      <div class="col-md-8 col-md-offset-1">
        @include('partials.messages')
          <div class="panel panel-primary">
              <div class="panel-heading">
                  <span>Agregar Celula</span>
              </div>

              <div class="panel-body">

{!! Form::open(['id'=>'idForm','route' =>['celulas.update',$cell->id],'method'=>'PUT']) !!}

<div class="col-xs-4">
  <label for="">Célula</label>
  <input type="text" class="form-control" name="cell" value="{{$cell->name}}" readonly="readonly">
</div>
<div id="celulaSector" class="form-group col-xs-12" >

  {!! Form::label('Sector') !!}
  <div class="row">
      <div id="divSectores" class="col-xs-4">
          <select name="sector_id" id="sector_id" class="form-control">
              <option value="-1"></option>
              <option value="0">------Agregar Sector------</option>
              @foreach($sectores as $sector)
                @if ($sector->id==$cell->sector_id)
                  <option selected="selected"value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                @else
                  <option value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                @endif

                  @endforeach
          </select>
      </div>

      <div id="sectorCelula" class="col-xs-4" style="display:none;">
          <input id="sectorC" name="sectorC" type="text"  class="form-control" placeholder="Ingrese el sector a agregar">
      </div>
      <div id="AgregarSectores" class="col-xs-4" style="display: none;">
          <button id="agregarSector" type="button" onclick="addSector()" class="btn btn-default btn-sm" title="Agregar Sector">
              <span class="glyphicon glyphicon-plus"></span>
          </button>
      </div>

  </div>
</div>

<div id="tipoCelula" class="form-group col-xs-6">
  <label>Tipo:</label>
  <select  name="tipoC" class="form-control">
      <option value="0"></option>
      @if ($cell->tipo=='Evangelista')
        <option selected="selected" value="Evangelista">Evangelista</option>
        <option value="Discipulado">Discipulado</option>
      @else
        <option value="Evangelista">Evangelista</option>
        <option value="Discipulado">Discipulado</option>
      @endif

  </select>
</div>

<div  id="divYes" class="form-group col-xs-6">

  {!!Form::label('Red a la que pertenece:')!!}
  <select class="form-control" name="red_idCelula" id="red_idCelula">
    @foreach($redes as $red)
        @if($red->id ==$cell->red_id)
            <option selected="selected" value="{{$red['id']}}">{{$red['nombre']}}</option>
        @else <option  value="{{$red['id']}}">{{$red['nombre']}}</option>
        @endif
        @endforeach
  </select>

  </div>

  <div class="col-md-6 col-md-offset-1">

      <button class="btn btn-success btn-md pull-right" type="submit">Editar</button>

  </div>
</div>

      {!! Form::close() !!}

      </div>
  </div>
</div>
</div>
</div>

@endsection
@section('scripts')
{!! Html::script('js/showDivs.js') !!}
@endsection
