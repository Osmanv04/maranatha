<div  id="panelLideres" class="col-xs-10 col-md-10" style="display: none;">
    <div class="panel panel-info">

        <div class="panel-heading">
            <strong>Lideres</strong>
        </div>

        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                <th class="col-md-4">Nombre</th>
                <th class="col-md-4">Apellido</th>
                <th class="col-md-4">Asignar Celula</th>
                </thead>

                <tbody>

                @foreach($lideres as $lider)
                    <tr>
                        <td>{{$lider->nombre}}</td>
                        <td>{{$lider->apellido}}</td>
                        <input hidden name="persona_id" id="persona_id" value="{{$lider->id}}">
                        <td> <button type="submit" name="AgregarLider" title="Agregar" class='btn btn-social-icon btn-info'>
                                <span class="glyphicon glyphicon-plus"></span>

                            </button>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>


    </div>

</div>
