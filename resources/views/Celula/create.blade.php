
@extends('layouts.master')

@section('title')

@section('content')
    {!!Html::style('css/example.css')!!}

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
            @include('partials.messages')
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span>Agregar Celula</span>
                    </div>

                    <div class="panel-body">

    {!! Form::open(['id'=>'idForm','route' =>'celulas.store','method'=>'POST']) !!}


    <div id="celulaSector" class="form-group col-xs-12" >

        {!! Form::label('Sector') !!}
        <div class="row">
            <div id="divSectores" class="col-xs-4">
                <select name="sector_id" id="sector_id" class="form-control">
                    <option value="-1"></option>
                    <option value="0">------Agregar Sector------</option>
                    @foreach($sectores as $sector)
                        <option value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                        @endforeach
                </select>
            </div>

            <div id="sectorCelula" class="col-xs-4" style="display:none;">
                <input id="sectorC" name="sectorC" type="text"  class="form-control" placeholder="Ingrese el sector a agregar">
            </div>
            <div id="AgregarSectores" class="col-xs-4" style="display: none;">
                <button id="agregarSector" type="button" onclick="addSector()" class="btn btn-default btn-sm" title="Agregar Sector">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </div>

        </div>
    </div>

    <div id="tipoCelula" class="form-group col-xs-6">
        <label>Tipo:</label>
        <select  name="tipoC" class="form-control">
            <option value="0"></option>
            <option value="Evangelista">Evangelista</option>
            <option value="Discipulado">Discipulado</option>
        </select>
    </div>

    <div  id="divYes" class="form-group col-xs-6">

        {!!Form::label('Red a la que pertenece:')!!}

        {!! Form::select('red_idCelula',$redes,null,['placeholder'=>'Selecciona una red','class'=>'form-control','id'=>'red_idCelula']) !!}

        </div>

        <div class="col-md-6 col-md-offset-1">

            <button class="btn btn-success btn-md pull-right" type="submit">Registrar</button>

        </div>
    </div>

            {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
    </div>

@endsection
@section('scripts')
    {!! Html::script('js/showDivs.js') !!}
@endsection
