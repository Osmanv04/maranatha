<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Listado de Células</title>
    <style>
        table, td, th {
            border: 1px solid #ddd;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 15px;
        }
        h3{
            text-align: center;
        }
        #page-wrap { padding: 80px; }
    </style>

</head>

<body>
<div id="page-wrap">
    <img src="./img/logo.png" style="margin-top:-50px;" align="center">
    <h3>Listado de Células</h3>
    <table >

        <tbody>
        <tr>
            <th >Nombre de celula</th>
            <th >Tipo</th>
            <th >Red</th>
            <th >Sector</th>
            <th >Lideres</th>
        </tr>

        @foreach($celulas  as $celula)
            <tr>
                <td>
                    {{$celula->name}}
                </td>

                <td>
                    {{$celula->tipo}}
                </td>

                <td>
                    {{$celula->red}}
                </td>

                <td>
                    {{$celula->sector}}
                </td>

                <td>
                    @foreach ($lideres as $lider)
                        @if ($lider->celula_id==$celula->id)
                            {{$lider->nombre}} {{$lider->apellido}}<br>
                        @endif
                    @endforeach

                </td>

            </tr>

        @endforeach
        </tbody>


    </table>


</div>
</body>
</html>