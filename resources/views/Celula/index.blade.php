@extends('layouts.master')

@section('title','Listado de células')

@section('content')
    {!!Html::style('css/example.css')!!}


@include('partials.messages')

{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            {!! Form::open(['route'=>'celulas.index','method'=>'GET']) !!}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="col-xs-8">
                        <button class="btn btn-info btn-flat" type="submit" title="Todos las celulas"  >
                            <span class="glyphicon glyphicon-home"></span> Lista Completa
                        </button>
                        @if($dato==null)
                            <a href="{{URL::to('PDFListadocelulas')}}" class="btn btn-success" type="button">Exportar PDF</a>
                        @else
                            <a href="{{URL::to('PDFListadocelulas',$dato)}}" class="btn btn-success" type="button">Exportar PDF</a>
                        @endif
                    </div>

                    <div class="input-group input-group-sm col-xs-offset-4">
                        <input type="text" name="red" class="form-control" placeholder="Ingrese sector,red o tipo" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-info btn-flat" type="submit" title="Buscar celula">
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                    </div>
                    {!! Form::close() !!}

                </div>

                <div class="panel-body">

                    <table class="table table-bordered">
                        <thead>
                        <th class="col-md-2">Nombre de celula</th>
                        <th class="col-md-2">Tipo</th>
                        <th class="col-md-2">Red</th>
                        <th class="col-md-2">Sector</th>
                        <th class="col-md-2">Lideres</th>
                        <th class="col-md-2">Accion</th>


                        </thead>
                        <tbody>


                        @foreach($celulas  as $celula)
                            <tr>
                                <td>
                                    {{$celula->name}}
                                </td>

                                <td>
                                    {{$celula->tipo}}
                                </td>

                                <td>
                                    {{$celula->red}}
                                </td>

                                <td>
                                    {{$celula->sector}}
                                </td>

                                <td>
                                    @foreach ($lideres as $lider)
                                        @if ($lider->celula_id==$celula->id)
                                            {{$lider->nombre}} {{$lider->apellido}}<br>
                                        @endif
                                    @endforeach

                                </td>

                                <td> <a type="button" href="{{route('celulas.edit',$celula->id)}}" title="Editar" class='btn btn-social-icon btn-info'>
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a type="button" href="{{route('celulas.show',$celula->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                        <i class="fa fa-trash"></i>
                                    </a>

                                </td>
                            </tr>

                        @endforeach
                        </tbody>


                    </table>

                    <div class="text-center">
                        {!! $celulas->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
    {!! Html::script('js/showDivs.js') !!}
@endsection
