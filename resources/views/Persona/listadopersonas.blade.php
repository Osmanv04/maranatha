<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
 <title>Listado de Integrantes</title>
 <style>
  table, td, th {
   border: 1px solid #ddd;
   text-align: center;
  }

  table {
   border-collapse: collapse;
   width: 100%;
  }

  th, td {
   padding: 15px;
  }
  h3{
   text-align: center;
  }
  #page-wrap { padding: 80px; }
 </style>

</head>

<body>
<div id="page-wrap">
 <img src="./img/logo.png" style="margin-top:-50px;" aling="center">
 <h3>Listado de Integrantes</h3>
 <table >
  <tbody>
  <tr>
   <th >Nombre</th>
   <th >Apellido</th>
   <th >Telefono</th>

   <th >Red</th>
  </tr>
  @foreach($personas as $persona)
   <tr>
    <td>{{$persona->persona}}

    </td>
    <td>{{$persona->apellido}}</td>

    <td>{{$persona->telefono}}</td>
    @if ($persona->red == null)
     <td>Ninguna</td>

    @else
     <td>{{$persona->red}}</td>
    @endif

   </tr>
  @endforeach

  </tbody>


 </table>
</div>
</body>
</html>