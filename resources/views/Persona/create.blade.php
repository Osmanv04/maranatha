@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}
   <!-- Main component for a primary marketing message or call to action -->
<div class="container">
  <div class="row">
    <div class="col-md-10">
      <div class="stepwizard col-md-offset-2">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step col-md-offset-2">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
      </div>
      <div class="stepwizard-step col-md-offset-2">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
      </div>
      <div class="stepwizard-step col-md-offset-2">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
      </div>
      <div class="stepwizard-step col-md-offset-2">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
      </div>
        <div class="stepwizard-step col-md-offset-2">
            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
        </div>
    </div>

          </div>
          @include('partials.messages')
  {!! Form::open(['id'=>'idForm','route' =>'persona.store','method'=>'POST']) !!}

   <div class="row setup-content" id="step-1">
        @include('Persona.partials.basicos')

       <div class="col-xs-6 col-md-offset-6">
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
        </div>
      </div>

    <div class="row setup-content" id="step-2">
        @include('Persona.partials.direccion')
        <div class="col-xs-6 col-md-offset-6">
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
        </div>

      </div>
    <div class="row setup-content" id="step-3">
        @include('Persona.partials.cargaF')

        <div class="col-xs-6 col-md-offset-6">
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Siguiente</button>
        </div>
      </div>
        <div class="row setup-content" id="step-4">
            @include('Persona.partials.iglesia')
            <div class="col-xs-6 col-md-offset-6">
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Siguiente</button>
            </div>
        </div>

        <div class="row setup-content" id="step-5">
            @include('Persona.partials.tlf')
            <div class="col-xs-6 col-md-offset-6">

                <button class="btn btn-success btn-lg pull-right" type="submit">Registrar</button>
            </div>
        </div>

    </div>

        {!! Form::close() !!}
</div>
</div>




@endsection

@section('scripts')
    {!!Html::script('js/step.js')!!}
    {!!Html::script('js/addField.js')!!}
    {!! Html::script('js/showDivs.js') !!}
@endsection
