<h3> Llene sus datos</h3>
<div class="col-xs-6 ">
    <div class="form-group">
        {!!Form::label('Nombre')!!}
        {!!Form::text('nombre',$persona->nombre,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Apellido') !!}
        {!! Form::text('apellido',$persona->apellido,['id'=>'apellido','class'=>'form-control','placeholder'=>'Ingrese el apellido']) !!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">

        {!! Form::label('Cédula') !!}
        {!! Form::text('cedula',$persona->cedula,['id'=>'cedula','class'=>'form-control','placeholder'=>'Ingrese la cédula']) !!}

    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">

        {!! Form::label('Fecha de nacimiento') !!}
        <input type="date" name="fecha_nac" class="form-control" value="{{$persona['fecha_nac']}}" >
    </div>
</div>

<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Ocupación') !!}
        {!! Form::text('ocupacion',$persona->ocupacion,['id'=>'ocupacion','class'=>'form-control','placeholder'=>'Ingrese su ocupacion']) !!}
    </div>
</div>

<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Estado Civil') !!}
        <select id = 'edocivil_id' name = 'edocivil_id' class = 'form-control' placeholder = "Seleccione su estado civil">
            @foreach($edociviles as $edocivil)
                @if($persona->edocivil_id==$edocivil->id)
                <option selected = "selected"  value={{$edocivil['id']}}>{{$edocivil['estado']}}</option>
                @else <option value={{$edocivil['id']}}>{{$edocivil['estado']}}</option>
            @endif
            @endforeach
        </select>

    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">

        {!! Form::label('Correo') !!}
        {!! Form::email('correo',$persona->correo,['id'=>'correo','class'=>'form-control','placeholder'=>'Ingrese el correo']) !!}

    </div>
</div>
