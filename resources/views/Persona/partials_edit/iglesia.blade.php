<h3>Datos eclesiásticos</h3>
<div class="form-group col-xs-2">
    {!! Form::label('Bautizado?') !!}
    <div>
        @if($persona->bautizo==1)
            <input type="radio" class="si" name="opcion" value="si" checked> Sí
            <input type="radio" class="no" name="opcion" value="no"> No
        @else
            <input type="radio" class="si" name="opcion" value="si" > Sí
            <input type="radio" class="no" name="opcion" value="no"checked> No
            @endif
    </div>
</div>

<div class="form-group col-xs-2">
    {!! Form::label('De otra Iglesia') !!}
    <div >
        @if($persona->otraIglesia==1)
        <input type="radio" class="si" name="opcion2" value="siI" checked> Sí
        <input type="radio" class="no" name="opcion2" value="noI"> No
            @else
            <input type="radio" class="si" name="opcion2" value="siI" > Sí
            <input type="radio" class="no" name="opcion2" value="noI" checked> No
            @endif
    </div>
</div>
<div id="Otra" class="form-group col-xs-3"  style="display: none;">
    {!! Form::label('Ministerio') !!}
    {!! Form::text('iglesia',$persona->otraIglesia,['id'=>'iglesia','placeholder'=>'Ingrese el nombre del ministerio','class'=>'form-control'] ) !!}
</div>

<div class="form-group col-xs-3">
    {!! Form::label('Tiempo en la iglesia') !!}
    {!! Form::date('tiempo',$persona->tiempoIglesia,['id'=>'tiempo','class'=>'form-control','placeholder'=>'Ingrese el tiempo en la iglesia']) !!}
</div>

<div class="form-group col-xs-2 ">
    {!! Form::label('Pertenece a alguna red?') !!}
    <div >
        @if($persona->status_red==1)
        <input type="radio" id="valorRed" class="siRed" name="opcion3" value="siRed" checked> Sí
        <input type="radio" id="valorRed" class="noRed" name="opcion3" value="noRed"> No
            @else
            <input type="radio" id="valorRed" class="siRed" name="opcion3" value="siRed"> Sí
            <input type="radio" id="valorRed" class="noRed" name="opcion3" value="noRed" checked> No
            @endif
    </div>
</div>
<div  id="divYes" class="form-group col-xs-3" style="display:none;">

    {!!Form::label('Red a la que pertenece:')!!}
    <select class="form-control" placeholder="Selecciona una red" id="red_id" name = "red_id">
        @foreach($redes as $red)
            @if($red->id ==$persona->red_id)
                <option selected="selected" value="{{$red['id']}}">{{$red['nombre']}}</option>
            @else <option  value="{{$red['id']}}">{{$red['nombre']}}</option>
            @endif
            @endforeach
    </select>
</div>

<div id="esLider" class="form-group col-xs-4" style="display: none">
    <label>Es líder de célula?</label>
    <div >
        @if($persona->esLider==1)
        <input type="radio" class="siLider" name="opcion5" value="siLider" checked> Sí
        <input type="radio" class="noLider" name="opcion5" value="noLider"> No
            @else <input type="radio" class="siLider" name="opcion5" value="siLider" > Sí
        <input type="radio" class="noLider" name="opcion5" value="noLider" checked> No
            @endif
    </div>
</div>

<div id="cell" name="cell" class="form-group col-xs-4" style="display: none;">
    {!! Form::label('Célula a la que pertenece') !!}
    <select class="form-control" name="celula_id" id="celula_id">
      @foreach ($celulas as $celula)
        @if ($celula->id==$persona->celula_id)
          <option selected="selected" value="{{$celula->id}}">{{$celula->name}}</option>
        @else
          <option value="{{$celula->id}}">{{$celula->name}}</option>
        @endif
      @endforeach
    </select>

</div>

<div id="divNo" class="form-group col-xs-4" style="display: none;">
    {!! Form::label('Desea pertenecer a una?') !!}
    <div >
        <input type="radio" class="siRed2" name="opcion4" value="siRed2"> Sí
        <input type="radio" class="no" name="opcion4" value="noRed"> No
    </div>
</div>

<div id="divYes2" class="form-group col-xs-3" style="display:none;">

    {!!Form::label('Red')!!}
    {!! Form::select('red_id2',$redes,null,['placeholder'=>'Selecciona una red','class'=>'form-control','id'=>'red_id2']) !!}

</div>
