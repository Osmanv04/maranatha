<div class="row">
    <h3>Telefonos de contacto</h3>
    <div class="form-group col-xs-4">
        {!!Form::label('Movil')!!}
        <input type="text" name="movil" id="movil" class="form-control" value="{{$tlf['movil']}}" placeholder="Ingrese el numero del movil">
    </div>
    <div class="form-group col-xs-4">
        {!!Form::label('Casa')!!}
        <input type="text" name="casa" id="casa" class="form-control" value="{{$tlf['casa']}}" placeholder="Ingrese el numero de la casa">
    </div>
</div>