<h3> Carga Familiar</h3>
 @include('alerts.errorajax')
 @include('alerts.successajax')
<div class="container">
  <div class="col-xs-10 col-md-10">
      <div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
          <strong> Familiar Agregado Correctamente.</strong>
      </div>
      <div id = "listaCarga" class="panel panel-info">

          <div class="panel-heading">
              <strong>Carga Familiar</strong>
              <div class="navbar navbar-right" style=" margin-top: 1px;">
                  <a type="button" href="{{URL::to('cargaf/crear',$persona->id)}}" class="btn btn-success btn-sm" style="margin-bottom: 1px; margin-top: -5px;margin-right: 8px;padding: 3px 20px;" >Agregar</a>
              </div>

          </div>

          <div class="panel-body">
              <table class="table table-responsive">
                  <thead>
                  <th class="col-md-3">Nombre</th>
                  <th class="col-md-3">Apellido</th>
                  <th class="col-md-3">Parentesco</th>
                  <th class ="col-md-3">Accion</th>
                  </thead>

                  <tbody>

                  @foreach($cargasF as $cargaf)
                      <tr>
                          <td>{{$cargaf->nombrep}}</td>
                          <td>{{$cargaf->apellidop}}</td>
                          <td>{{$cargaf->parentesco}}</td>
                          <td> <a type="button" href="{{route('cargaf.edit',$cargaf->id)}}" title="Editar" class='btn btn-social-icon btn-info'>
                                  <i class="fa fa-edit"></i>
                              </a>

                              <a type="button" href="{{route('cargaf.show',$cargaf->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                  <i class="fa fa-trash"></i>
                              </a>

                          </td>
                      </tr>
                  @endforeach
                  </tbody>

              </table>

          </div>


      </div>

      </div>

</div>
