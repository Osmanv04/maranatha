<h3>Direccion donde reside</h3>

<div class="form-group col-xs-6">
    {!! Form::label('Estado') !!}
    <select id="estado_id" name="estado_id" class = "form-control" placeholder="Seleccione el estado..." >
        @foreach($nombres as $nombre)
          @if ($nombre->id==$direccion->estado_id)
            <option selected= "selected" value={{$nombre['id']}}>{{$nombre['nombre']}}</option>
          @else
            <option value={{$nombre['id']}}>{{$nombre['nombre']}}</option>
          @endif

        @endforeach
    </select>
</div>

<div class="form-group col-md-6">
    <label>Municipio</label>
    <input type="text" id="municipio" name="municipio" class="form-control" placeholder="Ingrese el municipio" value="{{$direccion['municipio']}}">
</div>

<div class="form-group col-xs-6">
    {!!Form::label('Avenida ')!!}
    <input type="text" name="avenida" id="avenida" class="form-control" value="{{$direccion['avenida']}}" placeholder="Ingrese la avenida">
</div>



<div class="form-group col-xs-6" >

    {!! Form::label('Sector') !!}
    <div class="row">
        <div id="divSectores" class="col-xs-5">
            <select name="sector_id" id="sector_id" class="form-control">
                <option value="-1"></option>
                <option value="0">Agregar Sector</option>
                @foreach($sectores as $sector)
                    @if($sector->id==$direccion->sector_id)
                    <option selected="selected" value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                    @else
                        <option value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div id="sectorCelula" class="col-xs-4" style="display:none;">
            <input id="sectorC" name="sectorC" type="text"  class="form-control" placeholder="Ingrese el sector a agregar">
        </div>
        <div id="AgregarSectores" class="col-xs-1" style="display: none;">
            <button id="agregarSector" type="button" onclick="addSector()" class="btn btn-default btn-sm" title="Agregar Sector">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
        </div>

    </div>
</div>

<div class="form-group col-xs-6">
    {!!Form::label('Calle')!!}
    <input type="text" name="calle" id="calle" class="form-control" value="{{$direccion['calle']}}" placeholder="Ingrese la calle">
</div>

<div class="form-group col-xs-6">
    {!!Form::label('Nro de casa')!!}
    <input type="text" name="nroCasa" id="nroCasa" class="form-control" value="{{$direccion['nroCasa']}}" placeholder="Ingrese la avenida">

</div>
<div class = "form-group col-xs-6">
    {!! Form::label('Edificio') !!}
    <input type="text" name="edificio" id="edificio" class="form-control" value="{{$direccion['edificio']}}" placeholder="Ingrese el numero del edificio">
</div>
