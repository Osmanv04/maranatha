
<div id="panelCarga" class="panel panel-primary" style="display: none;">
    <div class="panel-heading">
        <span>Agregar Familiar</span>
    </div>

    <div class="panel-body" >

        {!! Form::open(['id'=>'idModalCarga','url'=>'cargaf/crear','method'=>'POST']) !!}


        <div class="form-group col-xs-4">
            {!! Form::label('Nombre') !!}
            {!! Form::text('nombrep',null,['id'=>'nombrep','class'=>'form-control','placeholder'=>'Ingrese el nombre del pastor']) !!}

        </div>

        <div class="form-group col-xs-4">
            {!! Form::label('Apellido') !!}
            {!! Form::text('apellidop',null,['id'=>'apellidop','class'=>'form-control','placeholder'=>'Ingrese el apellido del pastor']) !!}

        </div>
        <div class="form-group col-xs-4">
            {!! Form::label('Parentesco') !!}
            {!!Form::select('parentesco',["Padre"=>'Padre',"Madre"=>'Madre',"Esposo(a)"=>'Esposo(a)',"Hijo(a)"=>'Hijo(a)',
            "Hermano(a)"=>'Hermano(a)',"Otro"=>'Otro'],null,['id'=>'parentesco','class' =>'form-control'])!!}
        </div>

        <input type="hidden" name="persona_id" id="persona_id" value="{{$persona->id}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

    </div>
    <div class="modal-footer">

        <a href="#" id="Agregar" type="button" class="btn btn-primary">Agregar</a>
        <a type="button" class="btn btn-default" Onclick ="mostrarLista()">Cancelar</a>
    </div>
    {!! Form::close() !!}
</div>