<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Información de persona</title>
    <style>
        table, td, tr {
            border: 1px solid #ddd;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        tr, td {
            padding: 15px;
        }
        h3{
            text-align: center;
        }
        #page-wrap { padding: 80px; }
    </style>

</head>

<body>
<div id="page-wrap">
    <img src="./img/logo.png" style="margin-top:-50px;" aling="center">
    <h3>Datos básicos</h3>
<table>

    <tbody>

    @foreach ($personas as $persona)

        <tr>


            <td>
                <div>
                    <label>Nombre: </label>  {{$persona->nombre}}
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="">Apellido: </label>  {{$persona->apellido}}
                </div>

            </td>
            <td>
                <div class="form-group">
                    <label for="">Cédula: </label>  {{$persona->cedula}}
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="">Nacimiento: </label>  {{date("d-m-Y",strtotime($persona->fecha_nac))}}
                </div>
            </td>

        </tr>
        <tr>
            <td>
                <div>
                    <label for="">Ocupacion: </label>  {{$persona->ocupacion}}
                </div>
            </td>

            <td>
                <div>
                    <label for="">Estado civil: </label>  {{$persona->edocivil}}
                </div>
            </td>

            <td>
                <div>
                    <label for="">Correo: </label>  {{$persona->correo}}
                </div>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
<h3 class="text-center">Dirección</h3>
<table>
    <tbody>
    <tr>

        <td>
            <div >
                <label for="">Estado: </label>  {{$persona->estado}}
            </div>
        </td>
        <td>
            <div>
                <label for="">Municipio: </label>  {{$persona->municipio}}
            </div>
        </td>
        <td>
            <div>
                <label for="">Sector: </label>  {{$persona->nombreSector}}
            </div>
        </td>

        <td>
            <div>
                <label for="">Avenida: </label>@if ($persona->avenida=='')
                    No tiene
                @else {{$persona->avenida}}
                @endif
            </div>
        </td>

    </tr>
    <tr>
        <td>
            <div>
                <label for="">Calle: </label>  {{$persona->calle}}
            </div>
        </td>

        <td>
            <div>
                <label for="">Edificio: </label>@if ($persona->edificio=='')
                    No.
                @else
                    {{$persona->edificio}}
                @endif
            </div>
        </td>
        <td>
            <div>
                <label for="">Número de casa: </label> {{$persona->nroCasa}}
            </div>
        </td>
        <td>
        </td>
    </tr>
    </tbody>
</table>

<h3 class= "text-center">Carga Familiar</h3>
<table>
    <tbody>

    @foreach ($cargasF as $carga)
        <tr>

            <td>
                <div>
                    <label for="">Nombre: </label> {{$carga->nombrep}}
                </div>
            </td>

            <td>
                <div>
                    <label for="">Apellido: </label> {{$carga->apellidop}}
                </div>
            </td>

            <td>
                <div>
                    <label for="">Parentesco: </label> {{$carga->parentesco}}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<h3 class="text-center">Datos eclesiásticos</h3>
<table>
    <tbody>

    <tr>
        <td>
            <div class="form-group">
                <label for="">Bautizado: </label> @if ($persona->bautizo=='1')
                    Sí.
                @else
                    No.
                @endif
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="">De otra Iglesia: </label> @if ($persona->otraIglesia=='1')
                    {{$persona->otraIglesia}}
                @else
                    No.
                @endif
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="">Fecha en la Iglesia: </label> {{date("d-m-Y",strtotime($persona->tiempoIglesia))}}
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="">Red: </label> @if ($persona->red_id==null)
                    Ninguna.
                @else
                    {{$persona->red}}
                @endif
            </div>
        </td>


    </tr>
    <tr>
        <td>
            <div class="form-group">
                <label for="">Célula: </label> @if ($persona->celula_id ==null)
                    No.
                @else
                    {{$persona->celula}}
                @endif
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="">Líder de Célula</label> @if ($persona->esLider=='1')
                    Sí.
                @else
                    No.
                @endif
            </div>
        </td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<h3 class="text-center">Teléfonos de contacto</h3>
<table>
    <tbody>
    <tr>

        <td>
            <div class="form-group">
                <label for="">Teléfono móvil: </label> @if ($persona->telefono =='' or $persona->tlf_id ==null)
                    No posee.
                @else
                    {{$persona->telefono}}
                @endif
            </div>
        </td>
        <td>
            <div class="form-group">
                <label for="">Teléfono de casa: </label> @if ($persona->casa =='' or $persona->tlf_id ==null)
                    No posee.
                @else
                    {{$persona->casa}}
                @endif
            </div>
        </td>
    </tr>
    </tbody>
</table>
@endforeach
</div>
</body>
</html>