
<h3> Carga Familiar</h3>
<div id="contenedor">
    <div id="carga[]" class="row">
    <div class="form-group col-xs-4">
        {!!Form::label('Nombre')!!}
        {!!Form::text("nombrep[]",null,['id' => 'nombrep','class' => 'form-control','placeholder'=>'Ingrese nombre'])!!}
    </div>
    <div class="form-group col-xs-4">
        {!!Form::label('Apellido')!!}
        {!!Form::text("apellidop[]",null,['id' => 'apellidop','class' => 'form-control','placeholder'=>'Ingrese apellido'])!!}
    </div>


    <div class="form-group col-xs-4">
        {!! Form::label('Parentesco') !!}
        {!!Form::select('parentesco[]',["Padre"=>'Padre',"Madre"=>'Madre',"Esposo(a)"=>'Esposo(a)',"Hijo(a)"=>'Hijo(a)',
        "Hermano(a)"=>'Hermano(a)',"Otro"=>'Otro'],null,['id'=>'parentesco','class' =>'form-control'])!!}
    </div>

    </div>


</div>
<div class="form-group col-xs-6 col-md-offset-10">
    <button id="agregarCampo" type="button" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-plus"></span> Agregar
    </button>
    <button id="deleteField" type="button" class="eliminar btn btn-default btn-sm">
        <span class="glyphicon glyphicon-minus"></span> Eliminar
    </button>
</div>
