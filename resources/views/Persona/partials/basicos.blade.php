<h3> Llene sus datos</h3>
<div class="col-xs-6 ">
    <div class="form-group">
        {!!Form::label('Nombre')!!}
        {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese el nombre'])!!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Apellido') !!}
        {!! Form::text('apellido',null,['id'=>'apellido','class'=>'form-control','placeholder'=>'Ingrese el apellido']) !!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">

        {!! Form::label('Cédula') !!}
        {!! Form::text('cedula',null,['id'=>'cedula','class'=>'form-control','placeholder'=>'Ingrese la cédula']) !!}

    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">

        {!! Form::label('Fecha de nacimiento') !!}

        <input type="date" name="fecha_nac" id="fecha_nac" class="form-control"  autofocus="autofocus">

    </div>
</div>

<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Ocupación') !!}
        {!! Form::text('ocupacion',null,['id'=>'ocupacion','class'=>'form-control','placeholder'=>'Ingrese su ocupacion']) !!}
    </div>
</div>

<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Estado Civil') !!}
        <select id = 'edocivil_id' name = 'edocivil_id' class = 'form-control' placeholder = "Seleccione su estado civil">
            @foreach($edociviles as $edocivil)
                <option value={{$edocivil['id']}}>{{$edocivil['estado']}}</option>
            @endforeach
        </select>

    </div>
</div>

<div class="form-group col-xs-6">

    {!! Form::label('Correo') !!}
    {!! Form::text('correo',null,['id'=>'correo','class'=>'form-control','placeholder'=>'Ingrese el correo']) !!}

</div>


