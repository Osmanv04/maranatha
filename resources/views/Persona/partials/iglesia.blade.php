<h3>Datos eclesiásticos</h3>
<div class="form-group col-xs-2">
    {!! Form::label('Bautizado?') !!}
    <div>
        <input type="radio" class="si" name="opcion" value="si"> Sí
        <input type="radio" class="no" name="opcion" value="no"> No
    </div>
</div>

<div class="form-group col-xs-2">
    {!! Form::label('De otra Iglesia') !!}
    <div >
        <input type="radio" class="si" name="opcion2" value="siI"> Sí
        <input type="radio" class="no" name="opcion2" value="noI"> No
    </div>
</div>
<div id="Otra" class="form-group col-xs-3"  style="display: none;">
    {!! Form::label('Ministerio') !!}
    {!! Form::text('iglesia',null,['id'=>'iglesia','placeholder'=>'Ingrese el nombre del ministerio','class'=>'form-control']) !!}
</div>

<div class="form-group col-xs-3">
    {!! Form::label('Fecha que ingreso') !!}
    {!! Form::date('tiempo',null,['id'=>'tiempo','class'=>'form-control']) !!}
</div>

<div class="form-group col-xs-2 ">
    {!! Form::label('Pertenece a alguna red?') !!}
    <div >
        <input type="radio" id="valorRed" class="siRed" name="opcion3" value="siRed"> Sí
        <input type="radio" id="valorRed" class="noRed" name="opcion3" value="noRed"> No
    </div>
</div>
<div  id="divYes" class="form-group col-xs-3" style="display:none;">

    {!!Form::label('Red a la que pertenece:')!!}
    {!! Form::select('red_id',$redes,null,['placeholder'=>'Selecciona una red','class'=>'form-control','id'=>'red_id']) !!}

</div>

    <div id="esLider" class="form-group col-xs-4" style="display: none">
        <label>Es lider de celula?</label>
        <div >
            <input type="radio" class="siLider" name="opcion5" value="siLider"> Si
            <input type="radio" class="noLider" name="opcion5" value="noLider"> No
        </div>
    </div>

<div id="cell" name="cell" class="form-group col-xs-4" style="display: none;">
    {!! Form::label('Celula a la que pertenece') !!}
    {!! Form::select('celula_id',$celulas,null,['placeholder'=>'Selecciona una celula','class'=>'form-control','id'=>'celula_id']) !!}
</div>

<div id="divNo" class="form-group col-xs-4" style="display: none;">
    {!! Form::label('Desea pertenecer a una?') !!}
    <div >
    <input type="radio" class="siRed2" name="opcion4" value="siRed2"> Sí
    <input type="radio" class="no" name="opcion4" value="noRed"> No
    </div>
</div>

<div id="divYes2" class="form-group col-xs-3" style="display:none;">

    {!!Form::label('Red')!!}
    {!! Form::select('red_id2',$redes,null,['placeholder'=>'Selecciona una red','class'=>'form-control','id'=>'red_id2']) !!}


</div>
