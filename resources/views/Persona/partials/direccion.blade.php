<h3>Direccion donde reside</h3>
<div class="col-xs-6">
    <div class="form-group">
        {!! Form::label('Estado') !!}
        <select id="estado_id" name="estado_id" class = "form-control" placeholder="Seleccione el estado..." >
            @foreach($nombres as $nombre)
                <option value={{$nombre['id']}}>{{$nombre['nombre']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!!Form::label('Municipio')!!}
        {!!Form::text('municipio',null,['id'=>'municipio','class' => 'form-control','placeholder' =>'Ingrese el Municipio'])!!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!!Form::label('Avenida ')!!}
        {!!Form::text('avenida',null,['id'=>'nombre', 'class' => 'form-control', 'placeholder'=>'Ingrese la avenida'])!!}
    </div>

</div>
<div class="form-group col-xs-6" >

    {!! Form::label('Sector') !!}
    <div class="row">
        <div id="divSectores" class="col-xs-5">
            <select name="sector_id" id="sector_id" class="form-control">
                <option value="-1"></option>
                <option value="0">Agregar Sector</option>
                @foreach($sectores as $sector)
                    <option value="{{$sector->id}}">{{$sector->nombreSector}}</option>
                    @endforeach
            </select>
        </div>

        <div id="sectorCelula" class="col-xs-4" style="display:none;">
            <input id="sectorC" name="sectorC" type="text"  class="form-control" placeholder="Ingrese el sector a agregar">
        </div>
        <div id="AgregarSectores" class="col-xs-1" style="display: none;">
            <button id="agregarSector" type="button" onclick="addSector()" class="btn btn-default btn-sm" title="Agregar Sector">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
        </div>

    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!!Form::label('Calle')!!}
        {!!Form::text('calle',null,['id'=>'calle', 'class' => 'form-control', 'placeholder' => 'Ingrese la calle'])!!}
    </div>
</div>
<div class="col-xs-6">
    <div class="form-group">
        {!!Form::label('Nro de casa')!!}
        {!!Form::text('nroCasa',null,['id'=>'nroCasa', 'class' =>'form-control', 'placeholder' =>'Ingrese el número de la casa'])!!}

    </div>

</div>

<div class = "col-xs-6">
    <div class = "form-group">
        {!! Form::label('Edificio') !!}
        {!! Form::text('edificio',null,['id'=>'edificio', 'class'=>'form-control', 'placeholder' =>'Ingrese el nombre del edificio']) !!}
    </div>
</div>
