@extends('layouts.master')
@section('title')
@section('content')
    @include('partials.messages')

{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

                {!! Form::open(['route'=>'persona.index','method'=>'GET']) !!}
                  <div class="panel panel-primary">
                      <div class="panel-heading">
                          <div class="col-xs-8">
                              <button class="btn btn-info btn-flat" type="submit" title="Todos las personas"  >
                                  <span class="glyphicon glyphicon-home"></span> Lista Completa
                              </button>

                              @if($dato==null)
                              <a href="{{URL::to('PDFListadopersona')}}" class="btn btn-success" type="button">Exportar PDF</a>
                                  @else
                              <a href="{{URL::to('PDFListadopersonaF',$dato)}}" class="btn btn-success" type="button">Exportar PDF</a>
                                  @endif
                          </div>

                          <div class="input-group input-group-sm col-xs-offset-4">
                              <input type="text" name="dato" class="form-control" placeholder="Ingrese nombre,apellido o red" id="dato_buscado">
                                  <span class="input-group-btn">
                                    <button class="btn btn-info btn-flat" type="submit" title="Buscar celula">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                  </span>
                          </div>
                          {!! Form::close() !!}
                </div>

                <div class="panel-body">

                    <table class="table table-bordered">
                        <thead>
                        <th class="col-md-2">Nombre</th>
                        <th class="col-md-2">Apellido</th>
                        <th class="col-md-2">Telefono</th>

                        <th class="col-md-2">Red</th>
                        <th class="col-md-2">Opciones</th>
                        </thead>
                        <tbody>
                        @foreach($personas as $persona)
                            <tr>
                                <td>{{$persona->persona}}

                                </td>
                                <td>{{$persona->apellido}}</td>

                                <td>{{$persona->telefono}}</td>
                                @if ($persona->red == null)
                                  <td>Ninguna</td>

                              @else
                                <td>{{$persona->red}}</td>
                              @endif
                                <td> <a type="button" href="{{route('persona.edit',$persona->id)}}" title="Editar" class='btn btn-social-icon btn-info' >
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a type="button" href="{{route('persona.show',$persona->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                        <i class="fa fa-trash"></i>
                                    </a>

                                    <a type="button" href="{{URL::to('persona/mostrar',$persona->id)}}" title="Informacion" class="btn btn-social-icon btn-info">
                                        <i class="fa fa-info"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>


                    </table>

                    <div class="text-center">
                        {!! $personas->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{!!Html::script('js/jquery-1.12.2.js')!!}
{!!Html::script('js/bootstrap.min.js')!!}
{!!Html::script('js/step.js')!!}
{!!Html::script('js/addField.js')!!}

@endsection
