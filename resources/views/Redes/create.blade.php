@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}
  @include('partials.messages')

     {!! Form::open(['id'=>'idForm','route' =>'redes.store','method'=>'POST']) !!}


         <div class="col-xs-6 ">
             <div class="form-group">
               {!!Form::label('Nombre de la red')!!}
               {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control','placeholder'=>'Ingrese el nombre de la red'])!!}
             </div>
           </div>
           <h4 class="col-xs-12">Pastores</h4>
  <div id="contenedor" class="form-group col-xs-12">
          <div id="carga[]">

              <div class="form-group col-xs-4">
                  {!!Form::label('Nombre')!!}
                  <input type="text" name = "nombrepastor[]" id="campo_" class="form-control"placeholder="Ingrese el nombre ">
              </div>

              <div class="form-group col-xs-8">
                  {!!Form::label('Apellido')!!}
                  <div class="row">
                      <div class="col-xs-4">
                          <input type="text" name = "apellidopastor[]" id="campo_" class="form-control"placeholder="Ingrese el apellido ">
                      </div>
                      <div class="form-group col-xs-4" style="display:none;">
                          <i class="fa fa-times" aria-hidden="false"></i>
                      </div>
                  </div>

              </div>


          </div>


  </div>

     <div class="form-group col-xs-6 col-md-offset-8">
         <button id="agregarCampo" type="button" class="btn btn-default btn-sm">
             <span class="glyphicon glyphicon-plus"></span> Agregar
         </button>
     </div>

     <div class="col-xs-6 col-md-offset-6">
         <button class="btn btn-success btn-lg pull-right" type="submit">Registrar</button>
     </div>


           {!! Form::close() !!}



@endsection
@section('scripts')
    {!!Html::script('js/addPastores.js')!!}
    @endsection