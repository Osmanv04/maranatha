@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}
   <!-- Main component for a primary marketing message or call to action -->
<div class="container">
  <div class="row">
    <div class="col-md-10">

  {!! Form::open(['id'=>'idForm','route' =>['redes.destroy',$red->id],'method'=>'DELETE']) !!}
    <div class="form-group">
      <label for="exampleInputPassword1">Desea eliminar esta red:</label>
    </div>
    <div class="form-group">
      <label>Red</label>
      {{$red->nombre}}
    </div>

    {!!Form::submit('Eliminar',['name'=>'delete','id'=>'delete','content'=>'<span>Eliminar</span>',
    'class'=>'btn btn-danger btn-sm m-t-10'])!!}

    <a type="button" name="cancelar" href="{{route('redes.index')}}" class="btn btn-default btn-sm m-t-10">Cancelar</a>

        {!! Form::close() !!}
</div>
</div>
    </div>

@endsection
