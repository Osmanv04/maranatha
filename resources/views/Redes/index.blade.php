@extends('layouts.master')
@section('title')
@section('content')
    @include('partials.messages')

    {!!Html::style('css/example.css')!!}
    <!-- Main component for a primary marketing message or call to action -->
    <div class="container">
        <div class="row">
            <div class="col-md-10">

                {!! Form::open(['route'=>'redes.index','method'=>'GET']) !!}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="col-xs-8">
                            <button class="btn btn-info btn-flat" type="submit" title="Todos las redes"  >
                                <span class="glyphicon glyphicon-home"></span> Lista Completa
                            </button>
                            @if($dato==null)
                                <a href="{{URL::to('PDFListadoredes')}}" class="btn btn-success" type="button">Exportar PDF</a>
                            @else
                                <a href="{{URL::to('PDFListadoredes',$dato)}}" class="btn btn-success" type="button">Exportar PDF</a>
                            @endif
                        </div>

                        <div class="input-group input-group-sm col-xs-offset-4">
                            <input type="text" name="red" class="form-control" placeholder="Ingrese Red" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-info btn-flat" type="submit" title="Buscar Red"  >
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                    </div>

                    <div class="panel-body">

                        <table class="table table-bordered">
                            <thead>
                            <th class="col-md-4">Red</th>
                            <th class="col-md-4">Pastores</th>
                            <th class="col-md-4">Accion</th>


                            </thead>
                            <tbody>


                                @foreach($redes as $red)
                                <tr>
                                  <td>
                                      {{$red->nombre}}
                                    </td>

                                    <td>
                                      @foreach ($pastores as $pastor)
                                          @if ($pastor->red_id==$red->id)
                                            {{$pastor->nombre}} {{$pastor->apellido}}<br>
                                          @endif
                                      @endforeach

                                        </td>

                                    <td> <a type="button" href="{{route('redes.edit',$red->id)}}" title="Editar" class='btn btn-social-icon btn-info'>
                                            <i class="fa fa-edit"></i>
                                        </a>

                                        <a type="button" href="{{route('redes.show',$red->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                            <i class="fa fa-trash"></i>
                                        </a>

                                    </td>
                                </tr>

                            @endforeach
                            </tbody>


                        </table>

                        <div class="text-center">
                            {!! $redes->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
@section('scripts')
    {!!Html::script('js/step.js')!!}
    {!!Html::script('js/addField.js')!!}
@endsection
