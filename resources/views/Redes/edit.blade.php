@extends('layouts.master')

@section('title')

@section('content')
  {!!Html::style('css/example.css')!!}

<div class="container">
    @include('partials.messages')
     {!! Form::open(['id'=>'idForm','route' =>['redes.update',$redes->id],'method'=>'PUT']) !!}


         <div class="col-xs-6 ">
             <div class="form-group">
               {!!Form::label('Nombre de la red')!!}
                 <input type="text" name = "nombre" id="nombre" class="form-control"placeholder="Ingrese el nombre de la red" value="{{$redes['nombre']}}">

             </div>
           </div>

            @include('Redes.partials_pastores.createjs')

    <div class="col-xs-6 col-md-offset-6">
         <button class="btn btn-success btn-lg" type="submit">Editar</button>
     </div>


           {!! Form::close() !!}

</div>


@endsection

@section('scripts')

@endsection
