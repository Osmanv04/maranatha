<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Listado de Redes</title>
    <style>
        table, td, th {
            border: 1px solid #ddd;
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 15px;
        }
        h3{
            text-align: center;
        }
        #page-wrap { padding: 80px; }
    </style>

</head>

<body>
<div id="page-wrap">
    <img src="./img/logo.png" style="margin-top:-50px;" alig="center">
    <h3>Listado de Redes</h3>
    <table >

        <tbody>
        <tr>
            <th >Red</th>
            <th >Pastores</th>
        </tr>
        @foreach($redes as $red)
            <tr>
                <td>
                    {{$red->nombre}}
                </td>

                <td>
                    @foreach ($pastores as $pastor)
                        @if ($pastor->red_id==$red->id)
                            {{$pastor->nombre}} {{$pastor->apellido}}<br>
                        @endif
                    @endforeach

                </td>

            </tr>

        @endforeach
        </tbody>


    </table>


</div>
</body>
</html>