
<div class="col-xs-10 col-md-10">
    <div class="panel panel-info">

        <div class="panel-heading">
            <strong>Pastores</strong>
            <div class="navbar navbar-right" style=" margin-top: 1px;">

                <a type="button" href="{{URL::to('/pastor/crear',$redes->id)}}" class="btn btn-success" style="margin-bottom: 1px; margin-top: -5px;margin-right: 8px;padding: 3px 20px;">Agregar</a>
            </div>

        </div>

        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                <th class="col-md-4">Nombre</th>
                <th class="col-md-4">Apellido</th>
                <th class="col-md-4">Accion</th>
                </thead>

                <tbody>

                @foreach($pastores as $pastor)
                    <tr>
                        <td>{{$pastor->nombre}}</td>
                        <td>{{$pastor->apellido}}</td>
                        <td> <a type="button" href="{{route('pastor.edit',$pastor->id)}}" title="Editar" class='btn btn-social-icon btn-info'>
                                <i class="fa fa-edit"></i>
                            </a>

                            <a type="button" href="{{route('pastor.show',$pastor->id)}}" title="Eliminar" class='btn btn-social-icon btn-danger'>
                                <i class="fa fa-trash"></i>
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>


    </div>

    </div>

