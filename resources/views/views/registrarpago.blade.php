<div  id="pago-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 650px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="headerdetalle" class="col-lg-pull-4 modal-title">Registrar pago de pedido: <strong id="idpedido"></strong></h3>
            </div>

            <div class="modal-body">

            {!! Form::open(['id'=>'idForm']) !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token" >
           
            <div class="form-group">
                {!! Form::label('Código de transferencia:') !!}
                {!! Form::text('codigoOperacion',null,['id'=>'codigoOperacion','class'=>'form-control','placeholder'=>'Ingrese el codigo de operación bancaria']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Entidad Bancaria:') !!}
                {!! Form::text('nombre_banco',null,['id'=>'nombre_banco','class'=>'form-control','placeholder'=>'Ingrese nombre del banco utilizado para pagar']) !!}
            </div>

        {!! Form::close() !!}

                </div>
            <div class="modal-footer">
                <button class="btn" onclick="RegistrarPago(this);"  data-dismiss="modal" aria-hidden="true">Registrar Pago</button>

            </div><!-- /buttons -->
        </div>
    </div>
</div>