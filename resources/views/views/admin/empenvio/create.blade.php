@extends('layouts.admin')

@section('content')
    @include('alerts.error')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Nueva Empresa de envios</h3>
        </div>

        <div class="panel-body">
            {!! Form::open(['route'=>'empenvio.store','method'=>'POST']) !!}

                @include('admin.empenvio.form_empenv')

            {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>


@stop