@extends('layouts.admin')

@include('alerts.successajax')

@section('content')
    @include('admin.empenvio.edit')
    @include('common.msjeliminar')

    @include('admin.empenvio.listado')

@endsection
@section('scripts')
    {!!Html::script('js/scriptempenvio.js')!!}
    {!!Html::script('js/msjmodal.js')!!}
@endsection