<div class="form-group">
    {!! Form::label('Empresa de envio:') !!}
    {!! Form::text('nombre',null,['id'=>'nombre', 'class'=>'form-control','placeholder'=>'Nombre de empresa de envio']) !!}
</div>
<div class="form-group">
    {!! Form::label('Telefono de contacto:') !!}
    {!! Form::text('tlf_contacto',null,['id'=>'tlf_contacto', 'class'=>'form-control','placeholder'=>'Ingresa numero de contacto de la empresa']) !!}
</div>