<div class="empresasenv">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <SPAN> </SPAN>
            <h3 class="panel-title">
                Empresas de envios</h3>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="hidden" id="id">

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <th>ID</th>
                <th>Nombre</th>
                <th>Telefono</th>
                <th>Acciones</th>
                </thead>
                <tbody>
                @foreach($empresasenv as $emp)
                    <?php $ID=$emp->id ?>
                    <tr id="fila{{$ID }}">
                        <td> {{$ID}}</td>
                        <td class="col-sm-2" id="n{{$ID}}">{{$emp->nombre}}</td>
                        <td id="t{{$ID}}">{{$emp->tlf_contacto}}</td>
                        <td>
                            <button value="{{ $ID }}" OnClick='Mostrar(this);' title="Editar" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                                <i class="fa fa-edit"></i>
                            </button>
                            <button value="{{ $ID }}" OnClick='MsjEliminar(this,"Desea eliminar empresa envio ID: "+this.value+" ?");' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>

            {!! $empresasenv->render() !!}
        </div>

    </div>
</div>
</div>