<div class="libros">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <SPAN> </SPAN>
            <h3 class="panel-title">
                Listado de Libros  {!!link_to('./reportestock', $title='Exportar a PDF', $attributes = ['id'=>'actualizar', 'class'=>'btn btn-success'], $secure = null)!!}</h3>


        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="hidden" id="id">

        <div class="panel-body">
    <table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Titulo</th>
    <th>Categoria</th>
    <th>Autor</th>
    <th>Grado</th>
    <th>Publicado</th>
    <th>Precio</th>
    <th>Portada</th>
    <th>Cantidad Stock</th>
    <th>Acciones</th>
    </thead>
    <tbody>
    @foreach($libros as $libro)
        <?php $IDLibro=$libro->ID ?>
        <tr id="fila{{$IDLibro }}">
            <td> {{$IDLibro}}</td>
            <td class="col-sm-2" id="t{{$IDLibro}}">{{$libro->titulo}}</td>
            <td id="c{{$IDLibro}}">{{$libro->nombre_cat}}</td>
            <td id="a{{$IDLibro}}">{{$libro->nombre." ".$libro->apellido}}</td>
            <td id="g{{$IDLibro}}">{{$libro->grado}}</td>
            <td id="anop{{$IDLibro}}">{{$libro->anoPublicacion}}</td>
            <td id="precio{{$IDLibro}}">{{$libro->precio}}</td>
        <td>
            <img id="ptda{{$IDLibro}}" src="Portadas/{{$libro->url}}" alt="" style="width:80px;"/>
        </td>
            <td>
                <input id="cant{{$IDLibro}}" type="number"  value="{{$libro->cantdisponible}}">
            </td>
        <td>
            <button value="{{ $IDLibro }}" OnClick='Mostrar(this);' title="Editar" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                <i class="fa fa-edit"></i>
            </button>
            <button value="{{ $IDLibro }}" OnClick='MsjEliminar(this,"Desea eliminar el libro: "+this.value+" ?");' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                <i class="fa fa-trash"></i>
            </button>
        </td>
        </tr>

    @endforeach
        </tbody>
</table>

{!! $libros->render() !!}
</div>

    </div>
</div>
</div>