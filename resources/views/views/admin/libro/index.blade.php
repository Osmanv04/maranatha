@extends('layouts.admin')

    @include('alerts.successajax')

@section('content')
    @include('admin.libro.edit')
    @include('common.msjeliminar')

    @include('admin.libro.listado')

@endsection

@section('scripts')
    {!!Html::script('js/scriptlibro.js')!!}
    {!!Html::script('js/msjmodal.js')!!}
@endsection
