<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Libro</h4>
            </div>
                @include('alerts.errorajax')

            <div class="modal-body">
                {!!Form::open(['method'=>'POST','files' => true,'id'=>'formularioajax'])!!}
                    @include('admin.libro.form_libro')
                {!!Form::close()!!}
            </div>

            <div class="modal-footer">
                {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'actualizar', 'class'=>'btn btn-primary'], $secure = null)!!}
            </div>
        </div>
    </div>
</div>

{!!Html::script('js/vistaprevia.js')!!}


