<style>
    .thumb {
        height: 400px;
        width: 300px;
        border: 1px solid #000;
        margin: 10px 5px 0 0;
    }
</style>

<div class="row">
    <!--Seccion con campos de datos del libro-->
    <div  class="col-sm-4">
        <div class="form-group">
            {!!Form::label('titulo','Titulo:')!!}
            {!!Form::text('titulo',null,['id'=>'titulo','class'=>'form-control', 'placeholder'=>'Ingresa el Titulo del Libro'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('autor','Autor:')!!}

            <select id="idAutor" name="idAutor" placeholder="Seleccione Autor..." >
                @foreach($autores as $autor)
                    <option value={{$autor->id}}>{{$autor->nombre." ".$autor->apellido}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            {!!Form::label('grado','Grado:')!!}
            {!!Form::select('grado',["1ro"=>"1ro","2do"=>'2do',"3ro"=>'3ro',"4to"=>'4to',"5to"=>'5to',"6to"=>'6to'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('anoPublicacion','Año de Publicación:')!!}
            {!!Form::text('anoPublicacion',null,['id'=>'anoPublicacion','class'=>'form-control', 'placeholder'=>'Ingresa el año de publicaión'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('nroEdicion','Edición Numero:')!!}
            {!!Form::text('nroEdicion',null,['id'=>'nroEdicion','class'=>'form-control', 'placeholder'=>'Ingresa el Nro de edición'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('categoria','Categoria:')!!}
            {!!Form::select('idCategoria',$categorias)!!}
        </div>

        <div class="form-group">
            {!!Form::label('precio','Precio:')!!}
            {!!Form::text('precio',null,['id'=>'precio','class'=>'form-control', 'placeholder'=>'Ingresa el Precio del Libro'])!!}
        </div>

        <div class="form-group">
            {!!Form::label('cantidadstock','Cantidad Stock:')!!}
            {!!Form::text('cantstock',null,['id'=>'cantstock','class'=>'form-control', 'placeholder'=>'Ingrese cantidad disponible en Stock'])!!}
        </div>
    </div>

    <!--Seccion para cargar portada del libro-->
    <div class="col-sm-8 ">
            <div class="form-group">
                {!!Form::label('poster','Portada del Libro:')!!}
                <input type="file" id="url" name="url"/>

                <output id="list" data-estado="Nuevo">
                    <img class="thumb" src="../Portadas/LibroNuevo.png" alt="Portada del libro">
                </output>
            </div>
     </div>

</div>