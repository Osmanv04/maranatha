@extends('layouts.admin')
@section('content')
    @include('alerts.error')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo Libro</h3>
        </div>

        <div class="panel-body">
            {!!Form::open(['route'=>'libro.store', 'method'=>'POST','files' => true])!!}
                @include('admin.libro.form_libro')

            {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
            {!!Form::close()!!}
        </div>
    </div>

    {!!Html::script('js/vistaprevia.js')!!}
@endsection