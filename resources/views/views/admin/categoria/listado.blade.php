<div class="categorias">

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            Listado de Categorias</h3>
    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <input type="hidden" id="id">

    <div class="panel-body">

        <table class="table table-bordered table-striped">
            <thead>
                <th>ID</th>
                <th>Nombre de Categoria</th>
                <th class="col-md-2">Acciones</th>
            </thead>

            <tbody>
            @foreach($categorias as $cat)
                <?php $idcat=$cat->id ?>
                <tr id="fila{{$idcat}}">
                    <td>{{$idcat}}</td>
                    <td id="nom{{$idcat}}">{{$cat->nombre_cat}}</td>
                    <td>
                        <button value="{{ $idcat }}" OnClick='Mostrar(this);' title="Editar" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                            <i class="fa fa-edit"></i>
                        </button>
                        <button value="{{ $idcat }}" OnClick='MsjEliminar(this,"Desea eliminar la categoria ID: "+this.value+" ?");' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                            <i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $categorias->render() !!}

    </div>
</div>

    </div>