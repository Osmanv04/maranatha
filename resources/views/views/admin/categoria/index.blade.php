@extends('layouts.admin')

    @include('alerts.successajax')

@section('content')
    @include('admin.categoria.edit')
    @include('common.msjeliminar')

        @include('admin.categoria.listado')

@endsection

@section('scripts')
    {!!Html::script('js/scriptcategoria.js')!!}
    {!!Html::script('js/msjmodal.js')!!}
@endsection