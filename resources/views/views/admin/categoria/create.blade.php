@extends('layouts.admin')

@section('content')
    @include('alerts.error')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Nueva Categoria</h3>
        </div>

        <div class="panel-body">
            {!! Form::open(['route'=>'categoria.store','method'=>'POST']) !!}

                @include('admin.categoria.form_categoria')

            {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>

@stop