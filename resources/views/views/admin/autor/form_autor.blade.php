
<div class="form-group">
    {!! Form::label('Nombre:') !!}
    {!! Form::text('nombre',null,['id'=>'nombre', 'class'=>'form-control','placeholder'=>'Ingresa el nombre del autor']) !!}
</div>

<div class="form-group">
    {!! Form::label('Apellido:') !!}
    {!! Form::text('apellido',null,['id'=>'apellido', 'class'=>'form-control','placeholder'=>'Ingresa el apellido del autor']) !!}
</div>
