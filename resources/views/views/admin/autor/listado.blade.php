<div class="autores">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <SPAN> </SPAN>
            <h3 class="panel-title">
                Listado de Autores</h3>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="hidden" id="id">

        <div class="panel-body">
            <table class="table table-bordered table-striped">
                <thead>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th class="col-md-2">Acciones</th>
                </thead>

                <tbody>

                @foreach($autores as $autor)
                    <?php $idaut=$autor->id ?>
                    <tr id="fila{{$idaut}}">
                        <td id="idaut">{{$idaut}}</td>
                        <td id="nom{{$idaut}}">{{$autor->nombre}}</td>
                        <td id="ape{{$idaut}}">{{$autor->apellido}}</td>
                        <td>
                            <button value="{{ $idaut }}" OnClick='Mostrar(this);' title="Editar" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                                <i class="fa fa-edit"></i>
                            </button>
                            <button value="{{ $idaut }}" OnClick='MsjEliminar(this,"Desea eliminar el autor ID: "+this.value+" ?");' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

            {!! $autores->render() !!}

        </div>
    </div>
</div>