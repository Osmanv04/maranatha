@extends('layouts.admin')

@section('content')
    @include('alerts.error')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo Autor</h3>
        </div>

        <div class="panel-body">
            {!! Form::open(['route'=>'autor.store','method'=>'POST']) !!}

                @include('admin.autor.form_autor')

            {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>

@stop