@extends('layouts.admin')

    @include('alerts.successajax')

@section('content')
    @include('admin.autor.edit')
    @include('common.msjeliminar')

        @include('admin.autor.listado')

@endsection

@section('scripts')
    {!!Html::script('js/scriptautor.js')!!}
    {!!Html::script('js/msjmodal.js')!!}
@endsection