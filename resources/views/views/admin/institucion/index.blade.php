@extends('layouts.admin')

@include('alerts.successajax')
@section('content')

    @include('common.msjeliminar')
    @include('admin.institucion.listado')

@endsection
@section('scripts')
    {!!Html::script('js/msjmodal.js')!!}
    {!!Html::script('js/scriptsuscripcion.js')!!}

@endsection