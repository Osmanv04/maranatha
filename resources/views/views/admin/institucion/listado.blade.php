<div class="institutos">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <SPAN> </SPAN>
            <h3 class="panel-title">
                Solicitudes de Instituciones</h3>
        </div>

<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<input type="hidden" id="id">

<div class="panel-body">
    <table class="table table-bordered table-striped">
        <thead>
        <th>ID</th>
        <th class="col-md-2">Nombre</th>
        <th class="col-md-2">RIF</th>
        <th>Direccion</th>
        <th>Estado de suscripción</th>
        <th class="col-md-2">Acciones</th>
        </thead>

        <tbody>

        @foreach($instituciones as $instituto)
            <?php $ID=$instituto->id ?>
            <tr id="fila{{$ID}}">
                <td id="idinst">{{$ID}}</td>
                <td id="nom{{$ID}}">{{$instituto->nombre}}</td>
                <td id="rif{{$ID}}">{{$instituto->rif}}</td>
                <td id="dir{{$ID}}">{{$instituto->direccion}}</td>
                <td id="estado{{$ID}}">
                  @if($instituto->suscrito==0)
                    <strong>Por Aprobar</strong>
                    @else
                    <strong>Aprobado</strong>
                    @endif
                </td>
                <td id="btn{{$ID}}">
                    @if($instituto->suscrito==0)
                    <button  value="{{ $ID}}" OnClick='Aprobar(this);' title="Aprobar" class='btn btn-social-icon btn-primary'>
                        <i class="glyphicon glyphicon-ok-sign"></i>
                    </button>
                    <button value="{{ $ID }}" OnClick='Denegar(this);' title="Cancelar" class='btn btn-social-icon btn-danger'>
                        <i class="glyphicon glyphicon-remove-sign"></i>
                    </button>
                    @else
                        <button value="{{ $ID}}" OnClick='Aprobar(this);' title="Ver Detalles" class='btn btn-social-icon btn-primary'>
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

    {!! $instituciones->render() !!}

</div>
</div>
</div>

</div>
</div>