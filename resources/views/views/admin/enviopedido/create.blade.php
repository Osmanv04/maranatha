<div  id="enviopedido-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 650px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="headerdetalle" class="col-lg-pull-4 modal-title">Envío de pedido: <strong id="idpedido"></strong></h3>
            </div>

            <div class="modal-body">
                {!! Form::open(['id'=>'idForm']) !!}

                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token" >

                <div class="form-group">
                    {!! Form::label('Empresa de Envio:') !!}
                    <select id="idEmpresa" name="idEmpresa" placeholder="Empresa" >
                        @foreach($empresasenvio as $empresa)
                            <option value={{$empresa->id}}>{{$empresa->nombre." - ".$empresa->tlf_contacto}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('Código de rastreo:') !!}
                    {!! Form::text('codigorastreo',null,['id'=>'codigorastreo','class'=>'form-control','placeholder'=>'Ingrese el codigo de rastreo proporcionado por empresa de envios']) !!}
                </div>
                {!! Form::close() !!}
            </div>

            <div class="modal-footer">
                <button id="btnRegistrarEnvio"  class="btn btn-primary" onclick="RegistrarEnvio(this);"  data-dismiss="modal" aria-hidden="true">Registrar Envio</button>

            </div><!-- /buttons -->
        </div>
    </div>
</div>