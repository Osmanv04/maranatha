@extends('layouts.admin')

@section('content')
    @include('alerts.error')

    <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nuevo Usuario</h3>
            </div>

            <div class="panel-body">
            {!! Form::open(['route'=>'usuario.store','method'=>'POST']) !!}
                @include('admin.usuario.form_user')

            {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}

            {!! Form::close() !!}
            </div>
    </div>


@stop