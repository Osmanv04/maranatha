<div class="users">
<div class="panel panel-primary">
    <div class="panel-heading">
        <SPAN> </SPAN>
        <h3 class="panel-title">
            Listado de Administradores</h3>
    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <input type="hidden" id="id">

    <div class="panel-body">
        <table class="table table-bordered table-striped">
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th class="col-md-2">Acciones</th>
                </thead>

            <tbody>

    @foreach($users as $user)
        <?php $iduser=$user->id ?>
        <tr id="fila{{$iduser}}">
            <td id="iduser">{{$iduser}}</td>
            <td id="nom{{$iduser}}">{{$user->name}}</td>
            <td id="cor{{$iduser}}">{{$user->email}}</td>
            <td>
                <button value="{{ $iduser }}" OnClick='Mostrar(this);' title="Editar" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                    <i class="fa fa-edit"></i>
                </button>
                <button value="{{ $iduser }}" OnClick='MsjEliminar(this,"Desea eliminar el usuario ID: "+this.value+" ?");' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

{!! $users->render() !!}

</div>
    </div>
  </div>