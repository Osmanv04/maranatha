<!--para registrar automaticamente como usuario Administrativo-->
<input type="hidden" name="tipo" value="1">

<div class="form-group">
    {!! Form::label('Nombre de Usuario:') !!}
    {!! Form::text('name',null,['id'=>'name', 'class'=>'form-control','placeholder'=>'Ingresa el nombre de usuario']) !!}
</div>
<div class="form-group">
    {!! Form::label('Correo:') !!}
    {!! Form::email('email',null,['id'=>'email', 'class'=>'form-control','placeholder'=>'Ingresa el correo']) !!}
</div>
<div class="form-group">
    {!! Form::label('Password:') !!}
    {!! Form::password('password',['id'=>'password', 'class'=>'form-control','placeholder'=>'Ingresa su password']) !!}
</div>