@extends('layouts.admin')

    @include('alerts.successajax')
@section('content')
    @include('admin.usuario.edit')
    @include('common.msjeliminar')

    @include('admin.usuario.listado')

@endsection
@section('scripts')
    {!!Html::script('js/scriptuser.js')!!}
    {!!Html::script('js/msjmodal.js')!!}
@endsection