<div>_______________________________________________________________________________________________</div>
          <div style="text-align: center;" >Pedido #{{$pedido->IDPedido}}</div>


<h4> Fecha de Pedido:</h4>  {{date("d-m-Y",strtotime($pedido->fecha_pedido))}}
<h4> Facturado a Institución: </h4>{{$pedido->nombre}}

<div>_______________________________________________________________________________________________</div>

        <table id="order-table">
            <tr>
                 <th>Producto</th>
                 <th style="text-align: center; padding-right: 30px;">Cantidad</th>
                 <th>X</th>
                 <th>Precio Unitario</th>
                 <th>=</th>
                 <th style="text-align: center; padding-right: 30px;">Sub-Total</th>
            </tr>
             @foreach($librospedido as $libro)
            <tr class="odd">
                <td class="product-title">{{$libro->titulo}}</td>
                <td class="num-pallets"><input type="text" value="{{$libro->cantidad}}" class="num-pallets-input" id="sparkle-num-pallets"></td>
                <td class="times">X</td>
                <td class="price-per-pallet">BsF <span>{{$libro->precio}}</span></td>
                <td class="equals">=</td>
                <td class="row-total"><input type="text" class="row-total-input" value="BsF {{$libro->cantidad*$libro->precio}}" id="sparkle-row-total" disabled="disabled"></td>
            </tr>
            @endforeach
            <tr class="even">
            <td></td>
            <td></td>
            <td></td>
               <td></td>

                <td  style="text-align: right;">
                    TOTAL: 
                </td>
                <td><input type="text" class="total-box" value="BsF {{$pedido->montoaPagar}}" id="product-subtotal" disabled="disabled">
                </td>
            </tr>
        </table>


 <style type="text/css" style="display: none !important;">
    * {
        margin: 0;
        padding: 0;
    }
    body {
        overflow-x: hidden;
    }
    

    * { margin: 0; padding: 0; }
body { font: 12px "Lucida Grande", Helvetica, Sans-Serif; }
table { border-collapse: collapse; }
#page-wrap { padding: 80px; }

h1 { font: bold 40px Helvetica; letter-spacing: -2px; margin: 0 0 10px 0; }

.clear { clear: both; }

#order-table { width: 100%; }
#order-table td { padding: 5px; }
#order-table th { padding: 5px; background: black; color: white; text-align: left; }
#order-table td.row-total { text-align: right; }
#order-table td input { width: 75px; text-align: center; }
#order-table tr.even td { background: #eee; }
.num-pallets input { background: white; }
.num-pallets input.warning { background: #ffdcdc; }

#order-table td .total-box, .total-box { border: 3px solid green; width: 70px; padding: 3px; margin: 5px 0 5px 0; text-align: center; font-size: 14px; }

#shipping-subtotal { margin: 0; }

#shipping-table { width: 350px; float: right; }
#shipping-table td { padding: 5px; }

#shipping-table th { padding: 5px; background: black; color: white; text-align: left; }
#shipping-table td input { width: 69px;  text-align: center; }

#order-total { font-weight: bold; font-size: 21px; width: 110px; }
</style>
