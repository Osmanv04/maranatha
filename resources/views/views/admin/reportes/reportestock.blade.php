<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Reporte Stock Maralibros C.A.</title>
</head>

<body>

<div id="page-wrap">
    <img src="./images/logo.png"/>
    <div style="text-align: right;" ><strong>FECHA: {{date("d-m-Y",time())}}</strong></div>

    <div>_______________________________________________________________________________________________</div>
    <div style="text-align: center;" ><h2>Reporte de Stock</h2></div>
    <div>_______________________________________________________________________________________________</div>
    <div>_______________________________________________________________________________________________</div>

    <table id="order-table">
        <tr>
            <th>ID</th>
            <th>Titulo</th>
            <th>Categoria</th>
            <th>Grado</th>
            <th>Precio Unidad</th>
            <th>Cantidad Stock</th>
        </tr>

        <?php
            $totallibros=0;
        ?>

        @foreach($libros as $libro)
        <?php
            $totallibros+=$libro->cantdisponible;
            ?>

            <tr class="odd">
                <td >{{$libro->ID}}</td>
                <td >{{$libro->titulo}}</td>
                <td >{{$libro->nombre_cat}}</td>
                <td >{{$libro->grado}}</td>
                <td class="price-per-pallet">BsF <span>{{$libro->precio}}</span></td>
                <td class="row-total"><input type="text" class="row-total-input" value="{{$libro->cantdisponible}}" id="sparkle-row-total" disabled="disabled"></td>
            </tr>
        @endforeach
        <tr class="even">
            <td></td>
            <td></td>
            <td></td>

            <td></td>
            <td  style="text-align: right;">
                TOTAL:
            </td>
            <td><input type="text" class="total-box" value="{{$totallibros}}" id="product-subtotal" disabled="disabled">
            </td>
        </tr>
    </table>


</div>

<style type="text/css" style="display: none !important;">
    * {
    margin: 0;
    padding: 0;
    }
    body {
    overflow-x: hidden;
    }


    * { margin: 0; padding: 0; }
    body { font: 12px "Lucida Grande", Helvetica, Sans-Serif; }
    table { border-collapse: collapse; }
    #page-wrap { padding: 80px; }

    h1 { font: bold 40px Helvetica; letter-spacing: -2px; margin: 0 0 10px 0; }

    .clear { clear: both; }

    #order-table { width: 100%; }
    #order-table td { padding: 5px; }
    #order-table th { padding: 5px; background: black; color: white; text-align: left; }
    #order-table td.row-total { text-align: right; }
    #order-table td input { width: 75px; text-align: center; }
    #order-table tr.even td { background: #eee; }
    .num-pallets input { background: white; }
    .num-pallets input.warning { background: #ffdcdc; }

    #order-table td .total-box, .total-box { border: 3px solid green; width: 70px; padding: 3px; margin: 5px 0 5px 0; text-align: center; font-size: 14px; }

    #shipping-subtotal { margin: 0; }

    #shipping-table { width: 350px; float: right; }
    #shipping-table td { padding: 5px; }

    #shipping-table th { padding: 5px; background: black; color: white; text-align: left; }
    #shipping-table td input { width: 69px;  text-align: center; }

    #order-total { font-weight: bold; font-size: 21px; width: 110px; }
</style>
</body>

</html>