

<div  id="pedido-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="headerdetalle" class="col-lg-pull-4 modal-title">Detalle del pedido: <strong>{{$pedido[0]->IDPedido}}</strong></h3>
            </div>

            <div class="modal-body">
                @include('pedido.infopedido')
            </div>

            <div class="modal-footer">
                <button class="btn"  data-dismiss="modal" aria-hidden="true">Aceptar</button>


        @if($pedido[0]->status>=3)
                {!! link_to('#',$title='Descargar Factura PDF',$attributes=['id'=>'crearpdf','class'=>'btn btn-primary'],$secure =null) !!}
        @endif        

            </div><!-- /buttons -->
        </div>
    </div>
</div>

