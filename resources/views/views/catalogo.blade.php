@extends('layouts.principal')
@section('content')

    <style>
        .right {
            float: right;
            width: 350px;

            padding: 5px;
        }
    </style>

<body>

<input type="hidden" value="" id="filtroautor" >
<input type="hidden" value="" id="filtrogrado" >
<input type="hidden" value="" id="filtrocategoria" >

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-3 sidenav">


            <div style="background:#E8E8E8" class="panel-group" id="accordion">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                <i class="fa fa-book"></i>
                                 Categoria: <div id="catactual"></div><button >Todos</button>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body" >
                            @foreach($categorias as $cat)
                                <li>
                                    <a href="#headercatalogo" id="cc{{$cat->id}}" data-valor="{{$cat->id}}" style="cursor:pointer" OnClick="SetFiltroCategoria(this);">{{$cat->nombre_cat}}</a>
                                </li>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-user nav_icon"></i>Autor</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            @foreach($autores as $autor)
                                <li>
                                    <a id="au{{$autor->id}}" data-valor="{{$autor->id}}" OnClick="SetFiltroAutor(this);">{{$autor->nombre." ".$autor->apellido}}</a>
                                </li>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><i class="fa fa-pencil nav_icon"></i>Grado</a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <li>
                                <a id="1ro" OnClick="SetFiltroGrado(this);" href="#">Primer Grado</a>
                            </li>
                            <li>
                                <a id="2do" OnClick="SetFiltroGrado(this);" href="#">Segundo Grado</a>
                            </li>
                            <li>
                                <a id="3ro" OnClick="SetFiltroGrado(this);" href="#">Tercer Grado</a>
                            </li>
                            <li>
                                <a id="4to" OnClick="SetFiltroGrado(this);" href="#">Cuarto Grado</a>
                            </li>
                            <li>
                                <a id="5to" OnClick="SetFiltroGrado(this);" href="#">Quinto Grado</a>
                            </li>
                            <li>
                                <a id="6to" OnClick="SetFiltroGrado(this);" href="#">Sexto Grado</a>
                            </li>
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <div id="headercatalogo" class="col-md-9">
            <h4 class="">Catalogo de Libros</h4>
            <div  class=" input-group">
                <input type="text" class="" placeholder="Escriba un titulo">

                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"> </span>   Buscar
                </button>

            </div>


            @include('resultado')

        </div>
    </div>

</div>


</body>

@endsection
