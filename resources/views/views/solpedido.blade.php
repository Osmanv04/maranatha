@extends('layouts.principal')
@section('content')

    <body>
    <div class="container-fluid">
        <div class="row content">

            <div id="cuerpoizq" class="col-md-2" > </div>

            <div id="cuerpopedido" class="col-md-8" style="align-content: center">
                <h4>Realizar Pedido de <span id="numtlibros">{{$numdelibros}}</span> libros</h4>
                <hr>

                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th></th>
                        <th>Producto</th>
                        <th>En Stock</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Sub-total</th>
                        <th>&nbsp;</th>
                    </tr>

                    @foreach($libroscarrito as $libro)
                        <tr id="f{{$libro->ID}}">
                            <td>
                                <img src="Portadas/{{$libro->url}}" style="width:80px;"/>
                            </td>
                            <td style="width: 60%">
                                <strong>{{$libro->titulo}}</strong>
                            </td>
                            <td>
                                {{$libro->cantdisponible}} disponibles
                            </td>
                            <td >
                                <input style="width: 70px"  id="{{$libro->ID}}" onchange="ActualizarCantidadPedido(this)" value="{{$libro->cantidad}}"  max="{{$libro->cantdisponible}}" type="number">
                            </td>
                            <td id="precp{{$libro->ID}}" data-valor="{{$libro->precio}}">x <span >{{$libro->precio}}</span></td>
                            <td id="subtp{{$libro->ID}}">
                                {{$libro->cantidad*$libro->precio}}
                            </td>
                            <td>
                                <button onclick="EliminarLibroP(this);" class="btn btn-danger btn-mini removercart" value="{{$libro->ID}}" title="Remover">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total:</strong></td>
                        <td><span id='subtotalped' data-valor="{{$subtotal}}" class="badge badge-inverse">{{$subtotal}} BsF</span></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>

                <div class="modal-footer">
                <a class="btn btn-primary" onclick="ConfirmarPedido(this);">Confirmar Solicitud de Pedido</a>
                    </div>
            </div>

        </div><!--Fin de row-->
    </div><!--Fin de content fluid-->

    </body>

@endsection

{!!Html::script('js/scriptsolpedido.js')!!}