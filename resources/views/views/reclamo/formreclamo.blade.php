<div  id="reclamo-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 650px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="headerdetalle" class="col-lg-pull-4 modal-title">Reclamo del pedido: <strong>{{$pedido[0]->IDPedido}}</strong></h3>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <?php
                    $status="Desconocido";
                    if($pedido[0]->status==0)
                        $status="Esperando Aprobación";

                    if($pedido[0]->status==1)
                        $status="Aprobado. Registre su Pago";

                    if($pedido[0]->status==2)
                        $status="Pago Registrado. En Verificación";

                    if($pedido[0]->status==3)
                        $status="Pago Verificado. En Proceso de Envio";

                    if($pedido[0]->status==4)
                        $status="Enviado";
                    ?>
                    <div class="row">
                        <div class="col-sm-3" > Fecha de Pedido: <h4> {{date("d-m-Y",strtotime($pedido[0]->fecha_pedido))}} </h4> </div>
                        <div class="col-sm-3" > Estado del Pedido: <h4> {{ $status }} </h4></div>
                        <div class="col-sm-6" > Institución: <h4>{{$pedido[0]->nombre}}</h4></div>
                    </div>

                </div>
                <div class="panel-body">

            <div class="modal-body">




                <table class="table col-sm-12">

                    <tbody >
                    <tr>
                        <th style="width: 60%">Producto</th>
                        <th style="width: 20%">Unidades enviadas</th>
                        <th style="width: 20%">Cantidad Faltante</th>
                    </tr>

                    @foreach($librospedido as $libro)

                        <tr id="fila{{$libro->ID}}">
                            <td >
                                <input id="lBox{{$libro->ID}}" type="checkbox" name="lBox[]" value="{{$libro->ID}}"> {{$libro->titulo}}
                            </td>
                            <td >
                                <div id="cantidad" align="center">{{$libro->cantidad}}</div>
                            </td>
                            <td>
                                <input id="cantfaltante{{$libro->ID}}" style="width: 60px;" type="number" value="1">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div> </div> </div>

            <div class="modal-footer">
                    <button id="btnEnviarReclamo"  value="{{$pedido[0]->IDPedido}}" OnClick='EnviarReclamo(this);' title="EnviarReclamo" class='btn btn-social-icon btn-success'>
                        <i class="glyphicon glyphicon-ok-sign"></i> Enviar Reclamo
                    </button>
            </div><!-- /buttons -->
        </div>
    </div>
</div>