
<li><a href="#" data-toggle="modal" data-target="#cart-modal"><i class="glyphicon glyphicon-shopping-cart"></i> Carrito</a></li>

<li><a href="./pedidosinst"  ><i class="glyphicon glyphicon-list-alt"></i> Pedidos</a></li>

<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="glyphicon glyphicon-education"></i>
                {!! Auth::user()->name !!}

        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Ajustes</a>
            </li>
            <li class="divider"></li>
            <li><a href="{!!URL::to('logout')!!}"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a>
            </li>
        </ul>
    </li>


