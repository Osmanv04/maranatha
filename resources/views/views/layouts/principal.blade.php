<!DOCTYPE html>
<html>
<head>
    <title>MaraLibros C.A</title>

    @include('institucion.createModal')
    @include('usuario.login')
    @include('carrito.carrito')

    {!!Html::style('css/bootstrap.css')!!}
    {!!Html::style('css/style.css')!!}
    {!!Html::style('css/font-awesome.css')!!}

    {!!Html::script('js/jquery.min.js') !!}
    {!!Html::script('js/bootstrap.min.js') !!}
    {!!Html::script('js/scriptcatalogo.js')!!}


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Cinema Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
-->

</head>
<!-- header-section-starts -->
<!--<div class="full">-->

  <!--  <div class="main"> -->
        <div class="header">
            <div class="top-header">
                <div class="logo">
                    <a href="/"><img src="./images/logo4.png" alt="" /></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="./">Inicio</a></li>
                        <li><a href="./catalogo">Catalogo</a></li>
                        <li><a href="#contact">Contactenos</a></li>

                        @if(Auth::check())
                            @include('layouts.panellog')
                        @else
                            @include('layouts.panelinicial')
                        @endif

                    </ul>


                </div><!--/.nav-collapse -->


            </div>
            @yield('content')


    </div>

<body>


</body>





<div class="pie">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                   
                </div>
                <div class="col-md-4 text-center" style="margin-top:50px;">
                    <p class="textopie">Creado por: SoftMOB c.a &copy; 2016</p>
                    <p class="textopie"> Email: SoftMOB@gmail.com</p>
                    <p class="textopie">Números Telefónicos:  + 58 281-2769049</p>
                    <p class="textopie">                      + 58 412-9471756</p>
                </div>
                <div class="col-md-4">
                   
                </div>
            </div>
       
              
        </div> 
    </div>
</div>
 

<!--</div>-->

{!!Html::script('js/script.js') !!}
{!!Html::script('js/scriptcarrito.js') !!}
<div class="clearfix"></div>
</html>