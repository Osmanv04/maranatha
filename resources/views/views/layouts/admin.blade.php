<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/metisMenu.min.css')!!}
    {!!Html::style('css/sb-admin-2.css')!!}
    {!!Html::style('css/font-awesome.min.css')!!}
</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./admin">Panel Administrativo</a>
        </div>



        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span id="cantsus" class="badge">*</span>
                    Instituciones
                    <i class="glyphicon glyphicon-education"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{!!URL::to('/suscripciones')!!}"><i class="glyphicon glyphicon-asterisk"></i> Suscripciones por aprobar</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{!!URL::to('/institucion')!!}"><i class=" 	glyphicon glyphicon-th-list"></i> Listar Suscritas</a></li>
                </ul>
            </li>
        </ul>


        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span id="cantnp" class="badge">*</span>
                    Pedidos
                    <i class="glyphicon glyphicon-list-alt"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="{!!URL::to('/pedido')!!}"><i class="glyphicon glyphicon-asterisk"></i> Todos</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{!!URL::to('/reportepedidos')!!}"><i class=" 	glyphicon glyphicon-th-list"></i> Reporte Pedidos Pendientes</a>
                    </li>
                    <li>
                        <a href="{!!URL::to('/reporteganancia')!!}"><i class=" 	glyphicon glyphicon-th-list"></i> Reporte Ganancias Percibidas</a>
                    </li>
                </ul>
            </li>
        </ul>


        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <strong>Administrador:</strong>
                    @if(Auth::check())
                        {!! Auth::user()->name !!}
                    @endif <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Ajustes</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{!!URL::to('logout')!!}"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a>
                    </li>
                </ul>

        </ul>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Usuarios Administrativos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/usuario/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/usuario')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{!!URL::to('/categoria')!!}"><i class="fa fa-tags fa-fw"></i> Categorias<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/categoria/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/categoria')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{!!URL::to('/autor')!!}"><i class="fa fa-flash fa-fw"></i> Autores<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/autor/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/autor')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                    <li>
                        <a href="#"><i class="fa fa-book fa-fw"></i> Libros<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/libro/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/libro')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-plane fa-fw"></i> Empresas de Envio<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{!!URL::to('/empenvio/create')!!}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                            </li>
                            <li>
                                <a href="{!!URL::to('/empenvio')!!}"><i class='fa fa-list-ol fa-fw'></i> Listar Registradas</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
        </div>

    </nav>

    <div id="page-wrapper">
        @yield('content')
    </div>

</div>


{!!Html::script('js/jquery.min.js')!!}
{!!Html::script('js/bootstrap.min.js')!!}
{!!Html::script('js/metisMenu.min.js')!!}
{!!Html::script('js/sb-admin-2.js')!!}
{!!Html::script('js/adminscript.js')!!}

@section('scripts')
@show

</body>

</html>

<div class="navbar-header">
  <a class="navbar-brand" href="#"> <img src="img/logo.png" style="margin-top:-10px;" alt=""/> </a>
</div>
