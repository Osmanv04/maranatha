<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">¿Estas seguro?</h4>
            </div>

            <div class="modal-body">
                <div id='mensajeeliminar'></div>
            </div>
            <div class="modal-footer">

                <button class='btn btn-social-icon btn-primary' data-toggle='modal' data-target='#deleteModal'>
                    Cancelar
                </button>

                <button id='sieliminar' value="" OnClick='Eliminar(this);' title="Eliminar" class='btn btn-social-icon btn-danger' data-toggle='modal' data-target='#deleteModal'>
                    <i class="fa fa-trash"></i> Eliminar
                </button>

            </div>
        </div>
    </div>
</div>