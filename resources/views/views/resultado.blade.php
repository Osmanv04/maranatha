<div id="resultados">
    <?php $contador=0?>

    @foreach($libros as $libro)
            @if($contador==0)
                <div class="container">
                    <div class="row">
            @endif


    <div class="col-md-3">
        <ul class="galeria">
            <li>
                <a href="#img1"><img class="img-thumbnail" src="./Portadas/{{$libro->url}}"></a>
                <p class="textotitulos">{{$libro->titulo}}</p>
                <p class="textocategoria label label-danger">{{$libro->nombre_cat}}</p>
                <p class="textogrado label label-primary">{{$libro->grado}}</p>
                <p class="textoautor"><b>Autor: {{$libro->nombre." ".$libro->apellido}}</b></p>
                <p class="textocant">Cantidad disponible: <b>{{ $libro->cantdisponible }}</b></p>
                <p class="textoprec badge">{{$libro->precio}}BsF</p>



                <div class="boton" ><button class="btn" value="{{$libro->ID}}" OnClick="AñadirCarrito(this);" type="button">Enviar al Carrito</button></div>

                <?php $contador+=1;?>

            </li>
        </ul>
    </div>
            @if($contador==3)
                    </div>
                </div><br>
               <?php $contador=0; ?>
            @endif

    @endforeach

        {!!$libros->render()!!}
</div>
