
<style>
    .modal-header, .close {
        background-color: midnightblue;
        color:white !important;
        text-align: center;
        font-size: 30px;
    }
    .modal-footer {
        background-color: #f9f9f9;
    }
</style>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-lock"></span> Inicie Sesión</h4>
            </div>
            <div class="modal-body">
                {!!Form::open(['route'=>'log.store', 'method'=>'POST'])!!}
                <div class="form-group">
                    <span class="glyphicon glyphicon-user"></span>
                    {!!Form::label('correo','Correo:')!!}
                    {!!Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Ingresa tu correo'])!!}
                </div>
                <div class="form-group">
                    <span class="glyphicon glyphicon-eye-open"></span>
                    {!!Form::label('contrasena','Contraseña:')!!}
                    {!!Form::password('password',['class'=>'form-control', 'placeholder'=>'Ingresa tu contraseña'])!!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="iniciar" class="btn btn-primary"><span class="glyphicon glyphicon-off"></span> Iniciar</button>

                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>