<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token" >

<div class="panel-group">
    <div class="panel panel-info">
        <div class="panel-heading">Información básica</div>
        <div class="panel-body">

            <?php
            $status="Desconocido";
            if($pedido[0]->status==0)
                $status="Esperando Aprobación";

            if($pedido[0]->status==1)
                $status="Aprobado. Registre su Pago";

            if($pedido[0]->status==2)
                $status="Pago Registrado. En Verificación";

            if($pedido[0]->status==3)
                $status="Pago Verificado. En Proceso de Envio";

            if($pedido[0]->status==4)
                $status="Enviado";

            if($pedido[0]->status==5)

                $status="Recibido por Institucion";

            if($pedido[0]->status==6)
                $status="En Reclamo";
            ?>
            <div class="row">
                <div class="col-sm-3" > Fecha de Pedido: <h4> {{date("d-m-Y",strtotime($pedido[0]->fecha_pedido))}} </h4> </div>
                <div class="col-sm-3" > Estado del Pedido: <h4> {{ $status }} </h4></div>
                <div class="col-sm-6" > Institución: <h4>{{$pedido[0]->nombre}}</h4></div>
            </div>

        </div> </div>

    <div class="panel panel-info">
        <div class="panel-heading">Libros del pedido</div>
        <div class="panel-body">
            @include('pedido.listadopedido')
        </div>
    </div>

    @if($pago->count()==1)
    <div class="panel panel-info">
        <div class="panel-heading">Información de pago</div>
        <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4"> Fecha de Pago: <h4> {{date("d-m-Y",strtotime($pago['0']['fecha_pago']))}} </h4></div>
                    <div class="col-sm-4"> Código Transferencia: <h4> {{ $pago['0']['codigoOperacion'] }} </h4></div>
                    <div class="col-sm-4"> Entidad Bancaria: <h4> {{ $pago['0']['nombre_banco'] }} </h4></div>
                </div>

        </div>
    </div>
    @endif

    @if($datosenvio->count()==1)
    <div class="panel panel-info">
        <div class="panel-heading">Información de envío</div>
        <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4"> Fecha de Envio: <h4> {{date("d-m-Y",strtotime($datosenvio['0']['fecha_envio']))}} </h4></div>
                    <div class="col-sm-4"> Empresa De Envios: <h4> {{ $datosenvio['0']['nombre'] }} </h4></div>
                    <div class="col-sm-4"> Código de Rastreo: <h4> {{ $datosenvio['0']['cod_rastreo'] }} </h4></div>
                </div>

        </div>
    </div>
    @endif

    @if(!empty($reclamos))
    <div class="panel panel-info">
        <div class="panel-heading">Reclamo:</div>
        <div class="panel-body">
            <table class="col-sm-12">
                <thead>
                <tr>
                    <th style="width: 60%">Producto</th>
                    <th style="width: 40%">Unidades faltantes</th>
                </tr>
                </thead>
                <tbody >

                @foreach($reclamos as $libro)

                    <tr>
                        <td> {{$libro->titulo}} </td>
                        <td align="center">
                            {{$libro->cantidadFaltante}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
</div>
