@extends('layouts.admin')

@include('alerts.successajax')
@section('content')

    @include('common.msjeliminar')
    @include('pedido.pedidos')

@endsection
@section('scripts')
    {!!Html::script('js/msjmodal.js')!!}
    {!!Html::script('js/scriptpedido.js')!!}

@endsection