
<div  id="pedido-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document" style="width: 650px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="headerdetalle" class="col-lg-pull-4 modal-title">Detalle del pedido: <strong>{{$pedido[0]->IDPedido}}</strong></h3>
            </div>

            <div class="modal-body">

                @include('pedido.infopedido')
            </div>
            <div class="modal-footer">
                <!--//Aprobar Solicitud de pedido-->
                @if($pedido[0]->status==0)
                <button id="btnAprobar"  value="" OnClick='AprobarPedido(this);' title="Aprobar" class='btn btn-social-icon btn-success'>
                    <i class="glyphicon glyphicon-ok-sign"></i> Aprobar
                </button>
                <button value="" OnClick='DenegarPedido(this);' title="Cancelar" class='btn btn-social-icon btn-danger'>
                    <i class="glyphicon glyphicon-remove-sign"></i> Denegar
                </button>
                @endif
                    <!--Confirmar Pago-->
                    @if($pedido[0]->status==2)
                        <button id="btnConfirmarPago"  value="" OnClick='ConfirmarPago(this);' title="Confirmar Pago" class='btn btn-social-icon btn-success'>
                            <i class="glyphicon glyphicon-ok-sign"></i> Confirmar Pago
                        </button>
                    @endif


            </div><!-- /buttons -->
        </div>
    </div>
</div>