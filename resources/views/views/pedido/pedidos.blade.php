<div class="pedidos">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <SPAN> </SPAN>
            <h3 class="panel-title">
                Solicitudes de Pedidos</h3>
        </div>

        <div id="detallepedido">
            <!--Para el detalles de los pedidos-->
        </div>
        <div id="formenvio">
            <!--Para el formde envio-->
        </div>

<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<input type="hidden" id="id">

<div class="panel-body">
    <table class="table table-bordered table-striped">
        <thead>
        <th>ID</th>
        <th>Fecha</th>
        <th>Total de libros</th>
        <th>Monto</th>
        <th>Institución</th>
        <th >Estado</th>
        <th ></th>
        </thead>

        <tbody>

        @foreach($pedidos as $pedido)


            <?php
            $ID=$pedido->IDPedido;
            $fecha=date("d-m-Y",strtotime($pedido->fecha_pedido));

            $colorfila="";

            $status="Desconocido";
            if($pedido->status==0)
            {
                $status="Esperando Aprobación";
            }
            if($pedido->status==1)
            {
                $colorfila="info";
                $status="Aprobado. Registre su Pago";
            }
            if($pedido->status==2)
            {
                $colorfila="warning";
                $status="Pago Registrado. Esperando Confirmación";
            }
            if($pedido->status==3)
            {
                $colorfila="success";
                $status="Pago Verificado. Esperando Envio";
            }
            if($pedido->status==4)
            {
                $colorfila="warning";
                $status="Enviado";
            }
            if($pedido->status==5)
            {
                $colorfila="success";
                $status="Recibido por Institucion";
            }
            if($pedido->status==6)
            {
                $colorfila="danger";
                $status="En Reclamo";
            }
            ?>
            <tr class='{{$colorfila}}' id="fila{{$ID}}">
                <td id="idped{{$ID}}">{{$ID}}</td>
                <td id="fechap{{$ID}}">{{date("d-M-Y",strtotime($pedido->fecha_pedido))}}</td>
                <td id="totall{{$ID}}">{{$pedido->total_libros}}</td>
                <td id="montopagar{{$ID}}">{{$pedido->montoaPagar." BsF"}}</td>
                <td id="nominst{{$ID}}">{{$pedido->nombre}}</td>
                <td id="estado{{$ID}}">
                  @if($pedido->status==0)
                    <strong>Por Aprobar</strong>
                    @elseif($pedido->status==1)
                        <strong >Aprobado</strong>

                    @elseif($pedido->status==2)
                        <strong>Por Confirmar Pago</strong>
                    @elseif($pedido->status==3)
                        <strong>Pago Confirmado. Espera de Envio</strong>
                    @elseif($pedido->status==4)
                        <strong>Enviado</strong>
                    @elseif($pedido->status==5)
                        <strong>Recibido por Institución</strong>
                    @elseif($pedido->status==6)
                        <strong >Reclamo presentado</strong>
                    @endif
                </td>

                <td >
                        <button id="btn{{$ID}}" value="{{ $ID}}" OnClick='MostrarDetalle(this);' title="Ver Detalles" class='btn btn-social-icon btn-primary' data-montoapagar="{{$pedido->montoaPagar}}">
                            <i class="glyphicon glyphicon-eye-open"></i>
                        </button>

                    @if($pedido->status==3)
                        <button id="btn{{$ID}}" value="{{ $ID}}" OnClick='MostrarFormEnvio(this);' title="Realizar Envio" class='btn btn-social-icon btn-primary' data-montoapagar="{{$pedido->montoaPagar}}">
                            <i class="glyphicon glyphicon-send"></i>
                        </button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>



</div>
</div>
</div>

</div>
</div>