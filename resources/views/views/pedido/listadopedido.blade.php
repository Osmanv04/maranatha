<table class="col-sm-12">
   <thead>
    <tr>
        <th style="width: 60%">Producto</th>
        <th style="width: 13%">Unidades</th>
        <th style="width: 13%">Precio</th>
        <th style="width: 13%">Sub-total</th>
    </tr>
   </thead>
    <tbody >

       @foreach($librospedido as $libro)

        <tr id="fila{{$libro->ID}}">
            <td >
                {{$libro->titulo}}
            </td>
            <td >
                <div id="cantidad" align="center">{{$libro->cantidad}}</div>
            </td>
            <td id="prec{{$libro->ID}}" data-valor="{{$libro->precio}}">x <span>{{$libro->precio}}</span></td>
            <td id="subt{{$libro->ID}}">
                {{$libro->cantidad*$libro->precio}}
            </td>
        </tr>
    @endforeach

    <tr>
        <td></td>
        <td></td>
        <td><strong>Total:</strong></td>
        <td><span class="badge badge-inverse">{{$pedido[0]->montoaPagar." BsF"}}</span></td>
    </tr>
    </tbody></table>