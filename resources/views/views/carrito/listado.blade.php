
<table class="table table-striped">
    <tbody><tr>
        <th></th>
        <th>Producto</th>
        <th>En Stock</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th>Sub-total</th>
        <th>&nbsp;</th>
    </tr>

    @foreach($libroscarrito as $libro)
        <tr class="" id="fila{{$libro->ID}}">
            <td>
                <img id="ptda{{$libro->ID}}" src="Portadas/{{$libro->url}}" alt="" style="width:45px;"/>
            </td>
            <td style="width: 60%">
                <strong>{{$libro->titulo}}</strong>
            </td>
            <td>
                {{$libro->cantdisponible}} disponibles
            </td>
            <td >
                <input style="width: 70px"  id="{{$libro->ID}}" onchange="ActualizarCantidad(this)" value="{{$libro->cantidad}}" name="cantidad" type="number" max="{{$libro->cantdisponible}}">

            </td>
            <td id="prec{{$libro->ID}}" data-valor="{{$libro->precio}}">x <span >{{$libro->precio}}</span></td>
            <td id="subt{{$libro->ID}}">
                {{$libro->cantidad*$libro->precio}}
            </td>
            <td>
                <button onclick="EliminarCarrito(this);" class="btn btn-danger btn-mini removercart" value="{{$libro->ID}}" title="Remover">
                    <i class="glyphicon glyphicon-trash"></i>
                </button>
            </td>
        </tr>
    @endforeach

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><strong>Total:</strong></td>
        <td><span id='subtotalcart' class="badge badge-inverse">0</span></td>
        <td></td>
    </tr>
    </tbody></table>



<p class="clearfix">
      <span class="pull-right">

        

      </span>
</p>