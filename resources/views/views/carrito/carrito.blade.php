<div  id="cart-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document" style="width: 650px">
    <div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

    <img src="./images/logo4.png" class="pull-left bootic_cart_logo">

    <h3 id="headercarrito" class="col-lg-pull-4 modal-title"><strong id="numdelibros"></strong> unidades en el carro</h3>
    <div class="progress progress-striped active loading hide">
        <div class="bar" style="width: 100%;"></div>
    </div>
</div>

<input id="ultimoagregado" type="hidden">

<!--<div id='listacarrito' class="modal-body" style="overflow-x:hidden; overflow-y:scroll; height: 500px">-->
<div id='listacarrito' class="modal-body">

<!-- Aqui ira el listado de libros del carrito traido mendiante AJAX -->

</div><!-- /modal body -->
<div class="modal-footer">


        <input name="_method" value="put" type="hidden">
        <button class="btn"  data-dismiss="modal" aria-hidden="true">Seguir buscando</button>

        <!--<input name="cart_action"   value="Realizar Pedido »" data-loading-text="..." type="submit">-->
        <a class="btn btn-primary center " href="./confirmarpedido">Realizar Pedido</a>

        <div id="barraprogreso" class="progress-bar progress-bar-striped active" role="progressbar"
             aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:50%">
            Trabajando...
        </div>


</div><!-- /buttons -->
</div>
  </div>
  </div>
