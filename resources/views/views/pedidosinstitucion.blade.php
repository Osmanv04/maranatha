@extends('layouts.principal')
@section('content')
    {!! Html::script('js/scriptpedidouser.js') !!}

    @include('registrarpago')

    <div id="detallepedido">
        <!-- Para modal de detalles pedido -->
    </div>

    <div class="container-fluid">
        <div class="row content">

            <div class="col-sm-3 sidenav">


                <div style="background:#E8E8E8" class="panel-group" id="accordion">

                    <!--  <div class="panel panel-default">
                          <div class="panel-heading">
                              <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                      <i class="fa fa-book"></i>
                                      Menú:
                                  </a>
                              </h4>
                          </div>
                          <div id="collapse1" class="panel-collapse collapse in">
                              <div class="panel-body" >
                                      <li>
                                          <a href="#headercatalogo"  style="cursor:pointer" OnClick="SetFiltroCategoria(this);">Pedidos sin aprobar</a>
                                      </li>
                                      <li>
                                          <a href="#headercatalogo"  style="cursor:pointer" OnClick="SetFiltroCategoria(this);">Pedidos aprobados</a>
                                      </li>
                                  <li>
                                          <a href="#headercatalogo"  style="cursor:pointer" OnClick="SetFiltroCategoria(this);">Pedidos enviados</a>
                                  </li>
                                  <li>
                                          <a href="#headercatalogo"  style="cursor:pointer" OnClick="SetFiltroCategoria(this);">Pedidos en reclamo</a>
                                  </li>
                              </div>
                          </div>
                    </div>-->

                </div>



            </div>


            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            <input type="hidden" id="id">

            <!--En espera por aprobacion -->
            <div id="headerespera" class="col-md-8">

                <div class="panel panel-primary">
                    <div class="panel-heading" style="background: darkslategrey">
                        <SPAN> </SPAN>
                        <h3 class="panel-title">
                           Pedidos Recientes</h3>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <th>ID</th>
                            <th>Fecha Pedido</th>
                            <th>Total de libros</th>
                            <th>Monto</th>
                            <th>Estado</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($pedidos as $pedido)
                                <?php
                                        $ID=$pedido->IDPedido;
                                        $fecha=date("d-m-Y",strtotime($pedido->fecha_pedido));

                                        $colorfila="";

                                        $status="Desconocido";
                                        if($pedido->status==0)
                                        {
                                            $status="Esperando Aprobación";
                                        }
                                        if($pedido->status==1)
                                        {
                                            $colorfila="info";
                                            $status="Aprobado. Registre su Pago";
                                        }
                                        if($pedido->status==2)
                                        {
                                            $colorfila="warning";
                                            $status="Pago Registrado. Esperando Confirmación";
                                        }
                                        if($pedido->status==3)
                                        {
                                            $colorfila="success";
                                            $status="Pago Verificado. Esperando Envio";
                                        }
                                        if($pedido->status==4)
                                        {
                                            $colorfila="warning";
                                            $status="Enviado";
                                        }
                                if($pedido->status==5)
                                {
                                    $colorfila="success";
                                    $status="Recibido por Institucion";
                                }
                                if($pedido->status==6)
                                {
                                    $colorfila="danger";
                                    $status="En Reclamo";
                                }
                                ?>
                                <tr class='{{$colorfila}}' id="fila{{$ID }}">
                                    <td> {{$ID}}</td>
                                    <td>{{ $fecha  }}</td>
                                    <td>
                                        {{$pedido->total_libros}}
                                    </td>
                                    <td id="monto{{$ID}}">{{$pedido->montoaPagar." BsF"}}</td>
                                    <td> {{ $status }}</td>
                                    <td>
                                        <button id="btn{{$ID}}" value="{{ $ID }}" OnClick='MostrarDetalle(this);' title="Ver detalles" class='btn btn-social-icon btn-info' data-montoapagar="{{$pedido->montoaPagar}}">
                                            <i class="fa fa-eye"></i>
                                        </button>

                                        @if($pedido->status==1)
                                            <button value="{{ $ID }}" OnClick='MostrarFormPago(this);' title="Registrar Pago" class='btn btn-social-icon btn-warning'>
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                            </button>
                                        @endif

                                        @if($pedido->status==4)
                                        <button value="{{ $ID }}" OnClick='MostrarFormReclamo(this);' title="Enviar Reclamo" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                                            <i class="glyphicon glyphicon-fire"></i>
                                        </button>
                                        <button value="{{ $ID }}" OnClick='MarcarcomoRecibido(this);' title="Confirmar recepción de libros" class='btn btn-social-icon btn-info' data-toggle='modal' data-target='#editModal'>
                                            <i class="glyphicon glyphicon-ok-sign"></i>
                                        </button>
                                        @endif


                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection