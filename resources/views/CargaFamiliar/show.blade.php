@extends('layouts.master')

@section('title')

@section('content')
{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            {!! Form::open(['id'=>'idForm','route' =>['cargaf.destroy',$carga->id],'method'=>'DELETE']) !!}
            <div class="form-group">
                <label for="exampleInputPassword1">Desea borrar a este familiar:</label>
            </div>
            <div class="form-group">
                <label>Familiar: </label>
                {{$carga->nombrep}} {{$carga->apellidop}}
            </div>

            {!!Form::submit('Eliminar',['name'=>'delete','id'=>'delete','content'=>'<span>Eliminar</span>',
            'class'=>'btn btn-danger btn-sm m-t-10'])!!}

            <a type="button" name="cancelar" href="{{route('redes.edit',$carga->persona_id)}}" class="btn btn-default btn-sm m-t-10">Cancelar</a>

            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
