@extends('layouts.master')
@section('title')
@section('content')
@include('partials.messages')

{!!Html::style('css/example.css')!!}
        <!-- Main component for a primary marketing message or call to action -->
<div class="container">
    <div class="row">
        <div class="col-md-10">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span>Editar Familiar</span>
                </div>

                <div class="panel-body">

                    {!! Form::model($carga,['id'=>'idFormEdit','route' =>['cargaf.update',$carga->id],'method'=>'PUT']) !!}

                    <div class="form-group col-xs-4">
                        {!! Form::label('Nombre') !!}
                        {!! Form::text('nombrep',null,['id'=>'nombrep','class'=>'form-control','placeholder'=>'Ingrese el nombre del pastor']) !!}

                    </div>

                    <div class="form-group col-xs-4">
                        {!! Form::label('Apellido') !!}
                        {!! Form::text('apellidop',null,['id'=>'apellidop','class'=>'form-control','placeholder'=>'Ingrese el apellido del pastor']) !!}

                    </div>
                    <div class="form-group col-xs-4">
                        {!! Form::label('Parentesco') !!}
                        {!!Form::select('parentesco',["Padre"=>'Padre',"Madre"=>'Madre',"Esposo(a)"=>'Esposo(a)',"Hijo(a)"=>'Hijo(a)',
                        "Hermano(a)"=>'Hermano(a)',"Otro"=>'Otro'],null,['id'=>'parentesco','class' =>'form-control'])!!}
                    </div>




                </div>
                <div class="modal-footer">

                    <button class="btn btn-success btn-md" type="submit">Editar</button>
                    <a type="button" href="{{route('persona.edit',$carga->persona_id)}}" class="btn btn-default" onclick="stepCarga()">Cancelar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    {!!Html::script('js/step.js')!!}
    {!!Html::script('js/addField.js')!!}
@endsection