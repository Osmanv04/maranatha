<?php

use Illuminate\Database\Seeder;
use Maranatha\Models\Persona\EdoCivil;

class EdoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EdoCivil::create([
            'estado' =>'Soltero'
        ]);

        EdoCivil::create([
            'estado' =>'Casado'
        ]);

        EdoCivil::create([
            'estado' =>'Divorciado'
        ]);

        EdoCivil::create([
            'estado' =>'Viudo'
        ]);
    }
}
