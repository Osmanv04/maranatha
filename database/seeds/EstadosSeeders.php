<?php

use Illuminate\Database\Seeder;
use Maranatha\Models\Persona\Estado;
class EstadosSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create([
            "nombre" => 'Amazonas'
        ]);

        Estado::create([
            "nombre" => 'Anzoategui'
        ]);

        Estado::create([
            "nombre" => 'Apure'
        ]);

        Estado::create([
            "nombre" => 'Aragua'
        ]);

        Estado::create([
            "nombre" => 'Barinas'
        ]);

        Estado::create([
            "nombre" => 'Bolivar'
        ]);

        Estado::create([
            "nombre" => 'Carabobo'
        ]);

        Estado::create([
            "nombre" => 'Cojedes'
        ]);

        Estado::create([
            "nombre" => 'Delta Amacuro'
        ]);

        Estado::create([
            "nombre" => 'Distrito Capital '
        ]);

        Estado::create([
            "nombre" => 'Falcon'
        ]);

        Estado::create([
            "nombre" => 'Guarico '
        ]);

        Estado::create([
            "nombre" => 'Lara'
        ]);

        Estado::create([
            "nombre" => 'Merida'
        ]);

        Estado::create([
            "nombre" => 'Miranda'
        ]);

        Estado::create([
            "nombre" => 'Monagas'
        ]);

        Estado::create([
            "nombre" => 'Nueva Esparta'
        ]);

        Estado::create([
            "nombre" => 'Portuguesa'
        ]);

        Estado::create([
            "nombre" => 'Sucre'
        ]);

        Estado::create([
            "nombre" => 'Tachira'
        ]);

        Estado::create([
            "nombre" => 'Trujillo'
        ]);
        Estado::create([
            "nombre" => 'Vargas'
        ]);

        Estado::create([
            "nombre" => 'Yaracuy'
        ]);

        Estado::create([
            "nombre" => 'Zulia'
        ]);

    }
}
