<?php

use Illuminate\Database\Seeder;
use Maranatha\Models\Persona\Pastores;

class PastoresSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pastores::create([
            'nombre' => 'Omar',
            'apellido'=>'Paraguan',
            'red_id' =>'2'
        ]);

        Pastores::create([
            'nombre' => 'Javier',
            'apellido' => 'Reyes',
            'red_id' =>'2'
        ]);

        Pastores::create([
            'nombre' => 'Emely',
            'apellido'=>'Paraguan',
            'red_id' =>'3'
        ]);

        Pastores::create([
            'nombre' => 'Isabel',
            'apellido'=>'Ramos',
            'red_id' =>'3'
        ]);

        Pastores::create([
            'nombre' => 'Orlando',
            'apellido'=>'Alfonso',
            'red_id' =>'4'
        ]);

        Pastores::create([
            'nombre' => 'Marina',
            'apellido'=>'de Alfonso',
            'red_id' =>'4'
        ]);

        Pastores::create([
            'nombre' => 'Luis',
            'apellido'=>'Santaella',
            'red_id' =>'5'
        ]);

        Pastores::create([
            'nombre' => 'Erick',
            'apellido'=>'Castro',
            'red_id' =>'1'
        ]);

        Pastores::create([
            'nombre' => 'Marisela',
            'apellido'=>'de Castro',
            'red_id' =>'1'
        ]);

        Pastores::create([
            'nombre' => 'Pedro',
            'apellido'=>'Gonzalez',
            'red_id' =>'1'
        ]);

        Pastores::create([
            'nombre' => 'Yadirse',
            'apellido'=>'de Gonzalez',
            'red_id' =>'1'
        ]);

        Pastores::create([
            'nombre' => 'Pedro',
            'apellido'=>'Henning',
            'red_id' =>'6'
        ]);

        Pastores::create([
            'nombre' => 'Elina',
            'apellido'=>'de Henning',
            'red_id' =>'6'
        ]);

        Pastores::create([
            'nombre' => 'Cipriano',
            'apellido'=>'Hernandez',
            'red_id' =>'7'
        ]);

        Pastores::create([
            'nombre' => 'Aida',
            'apellido'=>'de Hernandez',
            'red_id' =>'7'
        ]);
    }
}
