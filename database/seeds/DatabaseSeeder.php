<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(UsersTableSeeder::class);
        $this->call(RedesSeeder::class);
        $this->call(PastoresSeeders::class);
        $this->call(EstadosSeeders::class);
        $this->call(EdoCivilSeeder::class);



        Model::reguard();

    }
}
