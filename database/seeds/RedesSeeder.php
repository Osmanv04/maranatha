<?php

use Illuminate\Database\Seeder;
use Maranatha\Models\Persona\Redes;

class RedesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Red 1
       $red = Redes::create([
           'nombre' => 'Empresarios'
       ]);

        //Red 2
        $red = Redes::create([
            'nombre' => 'Hombres'
        ]);

        //Red 3
        $red = Redes::create([
            'nombre' => 'Mujeres'
        ]);

        //Red 4
        $red = Redes::create([
            'nombre' => 'Jóvenes'
        ]);

        //Red 5
        $red = Redes::create([
            'nombre' => 'Niños y prejuveniles'
        ]);

        //Red 6
        $red = Redes::create([
            'nombre' => 'Urbana'
        ]);

        //Red 7
        $red = Redes::create([
            'nombre' => 'Matrimonios'
        ]);

    }
}
