<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePastoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pastores', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->string('apellido');
        $table->integer('red_id')->unsigned();
       // $table->integer('celula_id')->unsigned();
        $table->timestamps();
          //relaciones

          $table->foreign('red_id')->references('id')->on('red');
         // $table->foreign('celula_id')->references('id')->on('celulas');



      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pastores');
    }
}
