<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargaFamiliarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cargaFamiliar', function (Blueprint $table) {
        $table->increments('id');
        $table->string('parentesco');
        $table->string('nombrep');
        $table->string('apellidop');
        $table->integer('persona_id')->unsigned();
        $table->timestamps();
        //relaciones
        $table->foreign('persona_id')->references('id')->on('persona');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
