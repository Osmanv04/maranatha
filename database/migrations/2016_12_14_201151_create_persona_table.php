<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
      **/
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('cedula')->unique();
            $table->date('fecha_nac');
            $table->string('ocupacion');
            $table->boolean('bautizo');
            $table->boolean('esLider');
            $table->string('otraIglesia');
            $table->date('tiempoIglesia');

            $table->string('correo');
            $table->boolean('status_red');


            $table->integer('edocivil_id')->unsigned();
            $table->integer('direccion_id')->unsigned();
            $table->integer('red_id')->unsigned()->nullable();
            $table->integer('celula_id')->unsigned()->nullable();
            $table->integer('tlf_id')->unsigned();

            //relaciones

            $table->foreign('direccion_id')->references('id')->on('direccion')->onDelete('cascade');
            $table->foreign('red_id')->references('id')->on('red')->onDelete('set null');
            $table->foreign('tlf_id')->references('id')->on('telefono')->onDelete('cascade');
            $table->foreign('edocivil_id')->references('id')->on('edocivil')->onDelete('cascade');
            $table->foreign('celula_id')->references('id')->on('celulas')->onDelete('set null');


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     **/
    public function down()
    {
        Schema::drop('persona');
    }
}
