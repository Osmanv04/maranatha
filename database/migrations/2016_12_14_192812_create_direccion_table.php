<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('direccion', function (Blueprint $table) {
        $table->increments('id');
        $table->string('avenida');
        $table->string('calle');
        $table->integer('sector_id')->unsigned();
        $table->string('nroCasa');
        $table->string('edificio');
        $table->string('municipio');
        $table->integer('estado_id')->unsigned();
          $table->timestamps();

          //relaciones
          $table->foreign('estado_id')->references('id')->on('estado');
          $table->foreign('sector_id')->references('id')->on('sectors');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('direccion');
    }
}
